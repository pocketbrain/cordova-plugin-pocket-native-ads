# cordova-plugin-pocket-native-ads

This plugin takes care of the integration of Pocket Media native ads in your Cordova or Phonegap application.

## Installation
 cordova plugin add cordova-plugin-pocket-native-ads

## Usage
The plugin defines a global `pocket_native_ads` object.

To start inserting the native advertisements on a screen for which they ar configured, call the following function:

```
pocket_native_ads.init({
  applicationId: 'your_application_id*'
});
```

The applicationId can be retrieved from the Pocket Media native ads dashboard. There, you can also configure the native ads that should get inserted in your application.
