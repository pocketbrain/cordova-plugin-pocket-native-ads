(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Created by niek on 13/04/16.
 * This module fetches the ad configuration from the OfferEngine API
 */
var logger = require('../util/logger');
var ajax = require('../util/ajax');
var appSettings = require('../appSettings');
var deviceDetector = require('../device/deviceDetector');

/**
 * Fetch the ad configurations to load on the current page from the server
 * @param environment - The environment specific implementations
 * @param applicationId - The applicationId to fetch ad configurations for
 * @param callback - The callback to execute when the configurations are retrieved
 */
function fetchConfiguration(environment, applicationId, callback) {
    if(!applicationId) {
        logger.error('No application ID specified');
        return;
    }

    // We allow an override to specify the ad configurations directly in javascript, rather than
    // Having to fetch it from the server
    if(window.ADLIB_OVERRIDES && window.ADLIB_OVERRIDES.adConfigurations) {
        callback(null, window.ADLIB_OVERRIDES.adConfigurations);
        return;
    }

    var deviceDetails = deviceDetector.getDeviceDetails();

    var pathname = environment.getPathname();
    var formFactor = deviceDetails.formFactor;

    var requestUrl = appSettings.configurationsApiUrl;
    var queryParams = {
        path: pathname,
        device: formFactor,
        "application_id":applicationId
    };

    ajax.get({
        url: requestUrl,
        query: queryParams,
        success: function (data) {
            callback(null, data);
        },
        error: function (err) {
            callback(err, null);
        }
    });
}

module.exports = fetchConfiguration;

},{"../appSettings":8,"../device/deviceDetector":9,"../util/ajax":15,"../util/logger":17}],2:[function(require,module,exports){
/**
 * The main AdLibrary module
 */
var appSettings = require('./appSettings'),
    deviceDetector = require('./device/deviceDetector'),
    logger = require('./util/logger'),
    page = require('./page'),
    AdManager = require('./ads/adManager'),
    utils = require('./utils'),
    events = require('./events');

/**
 * Constructor that creates an instance of the AdLibrary object
 * @param environment - The environment object containing environment specific functions
 * @param [options] - Optional options to initialize the ad library with
 * @constructor
 */
var AdLib = function (environment, options) {
    this.applicationId = options && options.applicationId || this._getApplicationId();

    this.deviceDetails = deviceDetector.getDeviceDetails();
    this.environment = environment;
    this._adProvider = null;
    this.page = page;
    this.adManager = null;
    this.path = window.location.pathname;


    // If the script include is placed in the <head> of the page,
    // document.body is not ready yet, and we need it to retrieve the token
    if (document.body) {
        page.preloadToken(this.environment);
    }
    page.addDomReadyListener(this.environment);

    this._setUpPublicAPI();
};

/**
 * This function sets up the public API on the browsers' window object
 * so that publishers can interact with some functions of the Ad Library should they need to
 */
AdLib.prototype._setUpPublicAPI = function () {
    var instance = this;

    //noinspection JSUnusedGlobalSymbols
    window[appSettings.globalVarName] = {
        ready: true,
        /**
         * Load one or more adUnits manually
         * @param {string[]|string} adUnitNames - The names of the adUnits to remove
         */
        trigger: function (adUnitNames) {
            adUnitNames = utils.stringToArray(adUnitNames);
            instance.trigger(adUnitNames);
        },
        /**
         * Refresh the current ads on the page
         */
        refresh: function () {
            instance.refresh();
        },
        /**
         * Remove one or more ad units by specifying their names
         * @param {string|string[]} adUnitNames - the name(s) of the adUnits to remove
         */
        removeAdUnits: function (adUnitNames) {
            adUnitNames = utils.stringToArray(adUnitNames);
            instance.removeAdUnitsFromPage(adUnitNames);
        },
        /**
         * Remove all the current inserted ads from the page
         */
        removeAllAds: function () {
            instance.adManager.removeAllAds();
        },
        /**
         * Completely reload the ad library and re-read the ad configuration
         */
        reload: instance.reload.bind(instance),
        events: events
    };

    var messageData = {
        sender: appSettings.globalVarName,
        message: "ready"
    };

    //noinspection JSUnresolvedFunction
    window.postMessage(JSON.stringify(messageData), '*');
    logger.info("Public API initialized on var '" + appSettings.globalVarName + "'");
};

AdLib.prototype._getApplicationId = function () {
    var applicationId = null;
    if (typeof window.ADLIB_OVERRIDES !== "undefined" && window.ADLIB_OVERRIDES.applicationId) {
        applicationId = window.ADLIB_OVERRIDES.applicationId;
    }
    var scriptTag = document.querySelector("script[" + appSettings.applicationIdAttribute + "]");
    if (scriptTag) {
        applicationId = scriptTag.getAttribute(appSettings.applicationIdAttribute);
    }

    return applicationId;
};

/**
 * Initiates the ad library by reading the configuration file and starting
 * the process of placing ads on the page
 */
AdLib.prototype.init = function () {
    if (!this.applicationId) {
        logger.wtf('Could not retrieve applicationId');
        return; //Exit the application
    }

    this._initAdManager(this.applicationId);
};

/**
 * Remove all the ads from the current page and reload the configuration
 */
AdLib.prototype.reload = function () {
    this.adManager.removeAllAds();
    this.init();
};

/**
 * Creates a new instance of the ad manager and lets the manager starts its insertion
 * @private
 */
AdLib.prototype._initAdManager = function (applicationId) {
    this.adManager = new AdManager(applicationId, this.deviceDetails, this.environment);
    this.adManager.loadAds();

    this._startPathChangePolling();
};

/**
 * Starts polling window.location.path for changes and
 * refreshes the ads if a change is detected
 * @private
 */
AdLib.prototype._startPathChangePolling = function () {
    var self = this;
    setInterval(function () {
        if (window.location.pathname !== self.path) {
            //Refresh the AdLib if the Pathname has changed
            //This can be very common on single page applications using
            //The browser history API
            self.path = window.location.pathname;
            self.refresh();
        }
    }, 500);
};

/**
 * Remove the adUnits specified from the current page
 * @param {Array.<string>} adUnitNames - The names of the adUnits to remove
 */
AdLib.prototype.removeAdUnitsFromPage = function (adUnitNames) {
    this.adManager.removeAdUnits(adUnitNames);
};


/**
 * This method can be used for manually triggering a reload of the ads on the page
 *
 * For example, a single-page application does not reload the script when
 * changing the pages. The AdLib API exposed on the browser's window variable
 * Can use the refresh function to manually trigger a reload of the ads on a page
 */
AdLib.prototype.refresh = function () {
    this.adManager.refresh();
};

/**
 * Manually trigger certain ad units to be inserted on a page
 *
 * This is for adUnits with the trigger property specified
 * For example, when a dialog loads, ads in this dialog can be shown by using
 * the trigger method exposed by the AdLib API on the browser's window object
 *
 * @param {Array.<String>} unitTriggers - Array of the adUnit names to trigger
 */
AdLib.prototype.trigger = function (unitTriggers) {
    this.adManager.trigger(unitTriggers);
};


module.exports = AdLib;

},{"./ads/adManager":5,"./appSettings":8,"./device/deviceDetector":9,"./events":12,"./page":14,"./util/logger":17,"./utils":18}],3:[function(require,module,exports){
/**
 * Created by NiekKruse on 10/15/15.
 *
 * The ads builder module takes care of building the HTML structure of teh ad
 */
var logger = require('../util/logger'),
    appSettings = require('../appSettings');

/**
 * @typedef {Object} AdMacro - The macro object defining ad macros
 * @property {string} AdMacro.macro - The macro to search for in the string
 * @property {string} AdMacro.prop - The property in the object to replace the value with
 * The macros to replace for the actual advertisement
 * @type {Array.<AdMacro>}
 */
var adMacros = [
    {
        macro: "##campaign_name##",
        prop: "campaign_name"
    },
    {
        macro: "##campaign_description##",
        prop: "campaign_description"
    },
    {
        macro: "##click_url##",
        prop: "click_url"
    },
    {
        macro: "##category_name##",
        prop: "category_name"
    },
    {
        macro: "##campaign_image_url##",
        prop: "campaign_image"
    }
];

var childTagDictionary = {
    table: "tbody",
    tbody: "tr",
    theader: "tr",
    tr: "td"
};

/**
 * Replaces macros in a string with the properties of an object
 * @param s The string to replace the macros in
 * @param macros The macros to replace the string with
 * @param obj the object to get the macro properties from
 * @returns {String} - The string with the macros replaced
 * @private
 */
function _replaceMacros(s, macros, obj) {
    var regex = null;
    for (var i = 0; i < macros.length; i++) {
        var macro = macros[i];
        regex = new RegExp(macro.macro, "g");

        s = s.replace(regex, obj[macro.prop]);
    }

    return s;
}


//////////////////////////
//Start of API
//////////////////////////

/**
 * Creates a new instance of the adBuilder
 * @param ads array of ads provided by the adsFetcher
 * @constructor
 */
var AdBuilder = function (ads) {
    this._ads = ads;
    this._uid = 0;
};

/**
 * Replaces the macros for the actual advertisement
 * @param {string} htmlString - The html template string
 * @param ad the ad to use for the adUnit
 * @returns {String} the htmlString with replaced macros
 * @private
 */
AdBuilder.prototype._replaceAdMacros = function (htmlString, ad) {
    return _replaceMacros(htmlString, adMacros, ad);
};

/**
 * Creates an actual adUnit from adUnitSettings
 * @param adUnitSettings the settings for the adUnit
 * @param {HTMLElement} adContainerElement - The HTML element of the ad container
 * @returns {Node} the HTML Node for the ad unit
 */
AdBuilder.prototype.createAdUnit = function (adUnitSettings, adContainerElement) {
    var htmlString = adUnitSettings.htmlTemplate;
    var ad = this._ads.shift();

    if (!ad) {
        //We ran out of ads
        //TODO: what do in this case?
        logger.error("Ran out of ads before all ads could be inserted");
        return null;
    }

    //Create a temporary div to wrap the innerhtml im
    var tempDiv = document.createElement(adContainerElement.tagName);
    tempDiv.innerHTML = htmlString;

    //Get the htmlTemplate string as a DOM object
    var adElement = tempDiv.firstChild;
    adElement.style.position = "relative";
    adElement.className = appSettings.adElementClassname;
    adElement.id = this._newAdElementID();

    htmlString = tempDiv.innerHTML;
    htmlString = this._replaceAdMacros(htmlString, ad);

    tempDiv = document.createElement(adContainerElement.tagName);
    tempDiv.innerHTML = htmlString;

    adElement = tempDiv.firstChild;

    return adElement;
};

/**
 * Generates a new Unique ID for an ad unit
 * @returns {string} the new uniqueID
 * @private
 */
AdBuilder.prototype._newAdElementID = function () {
    return "pocket_adUnit_" + this._uid++;
};

/**
 * Create a new child element for a tag with a certain tag name
 * @param {HTMLElement} element - The element to create a child element for
 * @returns {HTMLElement} the child element
 * @private
 */
AdBuilder.prototype._createEmptyChildElement = function (element) {
    var elementTagName = element.tagName.toLowerCase();

    var tagNameToCreate = childTagDictionary[elementTagName] || 'div'; //Simply create a div it if it is not known in the dictionary
    return document.createElement(tagNameToCreate);
};

module.exports = AdBuilder;

},{"../appSettings":8,"../util/logger":17}],4:[function(require,module,exports){
/**
 * Created by NiekKruse on 10/19/15.
 *
 * Module that functions as a wrapper around the ad container element
 * Containing useful functions for finding the next position in an adContainer
 */
var AdContainer = function (adContainerSettings, containerElement) {
    this.containerElement = containerElement;
    this._currentIndex = adContainerSettings.startIndex;

    this.childElements = Array.prototype.slice.call(this.containerElement.children);
    var interval = adContainerSettings.interval;
    if (!interval) {
        interval = this._calculateInterval(adContainerSettings.maxNumberOfAds);
    }

    this._startIndex = adContainerSettings.startIndex;
    this._interval = interval;
};

/**
 * Calculate the interval for a unit where only a max number is set
 * @param maxNumberOfAds the max number of ads to ad to the parent container
 * @private
 */
AdContainer.prototype._calculateInterval = function (maxNumberOfAds) {
    var elements = this.childElements.slice(this._startIndex - 1);
    //TODO: maybe improve?
    return Math.round(elements.length / maxNumberOfAds);
};

/**
 * Get the next element after which an ad should be inserted
 * @returns {Node|null} - the HTML node to insert after, or null if it does not exist
 */
AdContainer.prototype.getNextElement = function () {
    if (this._currentIndex > this.childElements.length - 1) {
        return null;
    }

    var element = this.childElements[this._currentIndex];
    this._currentIndex += this._interval;
    
    return element;
};

/**
 * get the number of ads to insert in this adContainer
 * @returns {number} - the number of ads to insert
 */
AdContainer.prototype.getNumberOfAdsToInsert = function () {
    var index = this._startIndex;
    var counter = 0;

    while (this.childElements[index]) {
        counter++;
        index += this._interval;
    }

    return counter;
};

module.exports = AdContainer;

},{}],5:[function(require,module,exports){
/**
 * Created by NiekKruse on 10/16/15.
 *
 * The AdManager module takes care of anything ads related on the page and distributes tasks to the right modules
 */
var insertAds = require('./insertAds'),
    fetchConfiguration = require('../adConfiguration/fetchConfiguration'),
    fetchAds = require('./fetchAds'),
    page = require('../page'),
    logger = require('../util/logger'),
    events = require('../events');

/**
 * Creates a new instance of the adManager
 * @param applicationId - The ID of the application to receive ads for
 * @param deviceDetails - Details about the current users' device
 * @param environment - Environment specific functions.
 * @constructor
 */
var AdManager = function (applicationId, deviceDetails, environment) {
    this.applicationId = applicationId;
    this.deviceDetails = deviceDetails;
    this.environment = environment;
    this._currentAds = [];
    this._loadingAds = [];
    this._adsWithoutImages = [];
};

/**
 * Starts the adManager to detect which ads should be inserted
 * in the current context of the page and starts the insertion
 * of these ads
 */
AdManager.prototype.loadAds = function () {
    var self = this;
    this._getAdUnitsForCurrentPage(function (adUnits) {
        for (var i = 0; i < adUnits.length; i++) {
            var adUnit = adUnits[i];
            self.getAdsForAdUnit(adUnit);
        }
    });
};

/**
 * Retrieve ads for the given ad unit
 * @param adUnit - The ad unit to retrieve ads for
 */
AdManager.prototype.getAdsForAdUnit = function (adUnit) {
    var self = this;

    page.whenReady(function () {
        fetchAds(adUnit, function (ads) {
            if(ads.length === 0) {
                logger.error('No ads retrieved from OfferEngine');
                return; //Do not continue.
            }
                        
            self._onAdsLoaded(adUnit, ads);
        });
    });
};


/**
 * Remove all the currently inserted ads
 */
AdManager.prototype.removeAllAds = function () {
    this._removeInsertedAds(this._currentAds);
    this._currentAds = [];
};


/**
 * Manually trigger some adUnits to load
 * @param {string[]} triggers - The trigger(s) of the adUnits
 */
AdManager.prototype.trigger = function (triggers) {
    var adUnits = this.adConfig.getAdUnitsWithTrigger(triggers);

    if (adUnits.length > 0) {
        this._loadAdUnits(adUnits);
    } else {
        logger.warn("No AdUnits found with trigger(s): " + triggers.join(","));
    }
};

/**
 * Removes adUnits with given names from the page
 * @param {string[]} adUnitsToRemove - Array containing the names of the ad units to remove
 */
AdManager.prototype.removeAdUnits = function (adUnitsToRemove) {
    var currentAdsToRemove = this._currentAds.filter(function (details) {
        return adUnitsToRemove.indexOf(details.adUnit.name) > -1;
    });

    this._removeInsertedAds(currentAdsToRemove);
};

/**
 * Refreshes the ad library on the page
 */
AdManager.prototype.refresh = function () {
    this.removeAllAds();
    this.loadAds();
};

/**
 * Get the ad configuration for the current page
 * @private
 */
AdManager.prototype._getAdUnitsForCurrentPage = function (callback) {
    fetchConfiguration(this.environment, this.applicationId, function (err, adUnits) {
        logger.info('Received ' + adUnits.length + ' ad units to run on the current page');
        if (err) {
            logger.error('Could not fetch ad configuration.');
            return;
        }

        callback(adUnits);
    });
};

/**
 * Remove the given inserted ads on the page
 * @param currentAds - The current inserted ads to remove
 * @private
 */
AdManager.prototype._removeInsertedAds = function (currentAds) {
    for (var i = 0; i < currentAds.length; i++) {

        var currentAd = currentAds[i];
        for (var j = 0; j < currentAd.adElements.length; j++) {
            var adElementToRemove = currentAd.adElements[j];
            adElementToRemove.parentNode.removeChild(adElementToRemove);
        }
    }
};

/**
 * Callback that gets called when the ads are loaded from the AdProvider
 * @param {Object} adUnit - the adUnit to which the ads belong
 * @param {[]} ads - Array of ads obtained from the server
 * @private
 */
AdManager.prototype._onAdsLoaded = function (adUnit, ads) {
    var insertedAds = insertAds(adUnit, ads, this._adImageDoneLoading.bind(this));
    if (insertedAds) {
        this._currentAds.push({
            adUnit: adUnit,
            adElements: insertedAds
        });

        this._loadingAds = this._loadingAds.concat(insertedAds);
    }

    this._checkAllAdImagesDone();
};

/**
 * Callback that is executed each time the image of an ad is done loading
 * @param {HTMLElement} adElement - The element that is done loading
 * @param {boolean} hasImage - Boolean indicating whether the ad contained an image
 * @private
 */
AdManager.prototype._adImageDoneLoading = function (adElement, hasImage) {
    if (!hasImage) {
        this._adsWithoutImages.push(adElement);
    } else {
        var indexOfLoadingAd = this._loadingAds.indexOf(adElement);
        this._loadingAds.splice(indexOfLoadingAd, 1); //Remove from the loading ads array
    }

    this._checkAllAdImagesDone();
};

/**
 * Checks if all ad images are done loading and emits an event that all ads are ready
 * @private
 */
AdManager.prototype._checkAllAdImagesDone = function () {
    if ((this._adsWithoutImages.length === 0 && this._loadingAds.length === 0) || this._loadingAds.length === this._adsWithoutImages.length) {
        logger.info("All ads and images are done loading");
        var eventListeners = events.getListeners(events.events.afterAdsInserted);
        if (eventListeners && eventListeners.length) {
            for (var j = 0; j < eventListeners.length; j++) {
                eventListeners[j](this._currentAds);
            }
        }
    }
};


module.exports = AdManager;
},{"../adConfiguration/fetchConfiguration":1,"../events":12,"../page":14,"../util/logger":17,"./fetchAds":6,"./insertAds":7}],6:[function(require,module,exports){
/**
 * Created by niek on 13/04/16.
 *
 * This module provides ads to the library
 */
var ajax = require('../util/ajax');
var page = require('../page');
var logger = require('../util/logger');
var appSettings = require('../appSettings');

/**
 * Get the number of ads this unit needs to place all the ads on the page
 * @param adUnit The ad unit to get the required number of ads for
 * @returns {number} the number of required ads
 * @private
 */
function _getRequiredAdCountForAdUnit(adUnit) {
    var adContainers = page.getAdContainers(adUnit);

    if (!adContainers.length) {
        return 0;
    }

    var numberOfAdsToInsert = 0;
    for (var i = 0; i < adContainers.length; i++) {
        var adContainer = adContainers[i];
        numberOfAdsToInsert += adContainer.getNumberOfAdsToInsert();
    }

    return numberOfAdsToInsert;
}

/**
 * Request ads from the offerEngine
 * @param adUnit - The ad Unit that is requesting ads
 * @param callback - The callback to execute containing the ads
 */
function requestAds(adUnit, callback) {
    var limit = _getRequiredAdCountForAdUnit(adUnit);
    var token = page.getToken();

    var requestQuery = {
        "output": "json",
        "placement_key": adUnit.placementKey,
        "limit": limit,
        "token": token,
        "auto_device": 1
    };

    //noinspection JSUnresolvedVariable
    if (typeof ADLIB_OVERRIDES !== "undefined" && ADLIB_OVERRIDES.formFactor) {
        if (ADLIB_OVERRIDES.platform && ADLIB_OVERRIDES.fullDeviceName && ADLIB_OVERRIDES.version) {
            delete requestQuery.auto_device;
            requestQuery.os = ADLIB_OVERRIDES.platform;
            requestQuery.model = ADLIB_OVERRIDES.fullDeviceName;
            requestQuery.version = ADLIB_OVERRIDES.version;
        }
    }

    ajax.get({
        url: appSettings.adApiBaseUrl,
        query: requestQuery,
        success: function (data) {
            if (data.length !== limit) {
                logger.warn("Tried to fetch " + limit + " ads, but only received " + data.length);
            }

            callback(data);
        },
        error: function (e) {
            logger.wtf('An error occurred trying to fetch ads');
        }
    });
}

module.exports = requestAds;
},{"../appSettings":8,"../page":14,"../util/ajax":15,"../util/logger":17}],7:[function(require,module,exports){
/**
 * Created by niek on 13/04/16.
 * This module takes care of the ad insertion for a given ad unit
 */
var page = require('../page');
var AdBuilder = require('./adBuilder');
var deviceDetector = require('../device/deviceDetector');
var logger = require('../util/logger');

/**
 * Insert advertisements for the given ad unit on the page
 * @param adUnit - The ad unit to insert advertisements for
 * @param ads - Array of ads retrieved from OfferEngine
 * @param adLoadedCallback - Callback to execute when the ads are fully loaded
 * @returns {Array}
 */
function insertAds(adUnit, ads, adLoadedCallback) {
    var adContainers = page.getAdContainers(adUnit);

    if (!adContainers.length) {
        logger.error("No ad containers could be found. stopping insertion for adUnit " + adUnit.name);
        return []; //Ad can't be inserted
    }
    
    var adBuilder = new AdBuilder(ads);

    var beforeElement;
    var insertedAdElements = [];
    for (var i = 0; i < adContainers.length; i++) {
        var adContainer = adContainers[i];
        while ((beforeElement = adContainer.getNextElement()) !== null) {
            var adToInsert = adBuilder.createAdUnit(adUnit, adContainer.containerElement);

            if (adToInsert === null) {
                //we ran out of ads.
                break;
            }

            insertedAdElements.push(adToInsert);
            beforeElement.parentNode.insertBefore(adToInsert, beforeElement.nextSibling);

            // var elementDisplay = adToInsert.style.display || "block";
            //TODO: Why are we defaulting to block here?
            // adToInsert.style.display = elementDisplay;
            handleImageLoad(adToInsert, adLoadedCallback);
        }
    }

    return insertedAdElements;
}

/**
 * Add an event handler to the onload of ad images.
 * @param adElement - The HTML element of the advertisement
 * @param adLoadedCallback - Callback to execute when ads are loaded
 */
function handleImageLoad(adElement, adLoadedCallback) {
    var adImage = adElement.querySelector("img");
    if (adImage) {
        (function (adToInsert, adImage) {
            adImage.onload = function () {
                adLoadedCallback(adToInsert, true);
            };
        })(adElement, adImage);
    } else {
        adLoadedCallback(adElement, false);
    }
}

module.exports = insertAds;
},{"../device/deviceDetector":9,"../page":14,"../util/logger":17,"./adBuilder":3}],8:[function(require,module,exports){
/**
 * Created by NiekKruse on 10/5/15.
 * This module contains library wide debug settings
 * A module that exposes the app settings
 */
var enumerations = require("./util/enumerations");

/**
 * Exports the appSettings
 */
module.exports = {
    configFileName: "/adconfig.json",
    isDebug: false,
    logLevel: enumerations.logLevel.debug,
    adApiBaseUrl: "http://offerwall.12trackway.com/ow.php",
    globalVarName: "pocket_native_ads",
    xDomainStorageURL: " http://offerwall.12trackway.com/xDomainStorage.html",
    tokenCookieKey: "pm_offerwall",
    loggerVar: "__adlibLog",
    defaultSmartPhoneWidth: 375,
    defaultTabletWidth: 768,
    configurationsApiUrl: 'http://offerwall.12trackway.com/ow.php',
    applicationIdAttribute: 'data-application-id',
    adElementClassname: 'pm_native_ad_unit',
    displaySettings: {
        mobile: {
            minWidth: 0,
            maxWidth: 415
        },
        tablet: {
            minWidth: 415,
            maxWidth: 1024
        }
    }
};

},{"./util/enumerations":16}],9:[function(require,module,exports){
/**
 * Created by NiekKruse on 10/9/15.
 *
 * This module contains functionality for detecting details about a device
 */
var appSettings = require('../appSettings'),
    DeviceEnumerations = require('../device/enumerations');

/**
 * Check if the platform the user is currently visiting the page with is valid for
 * the ad library to run
 * @returns {boolean}
 */
function isValidPlatform() {
    if (typeof ADLIB_OVERRIDES !== "undefined" && ADLIB_OVERRIDES.platform) {
        return true; //If a platform override is set, it's always valid
    }
    return /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
}

/**
 * Detects the device form factor based on the ViewPort and the deviceWidths in AppSettings
 * The test_old is done based on the viewport, because it is already validated that a device is Android or iOS
 * @returns {*} the form factor of the device
 * @private
 */
function detectFormFactor() {
    var viewPortWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

    var displaySettings = appSettings.displaySettings; //convenience variable
    var formFactor;

    if (viewPortWidth >= displaySettings.mobile.minWidth && viewPortWidth <= displaySettings.mobile.maxWidth) {
        formFactor = DeviceEnumerations.formFactor.smartPhone;
    } else if (viewPortWidth >= displaySettings.tablet.minWidth && viewPortWidth <= displaySettings.tablet.maxWidth) {
        formFactor = DeviceEnumerations.formFactor.tablet;
    } else {
        formFactor = DeviceEnumerations.formFactor.desktop;
    }

    return formFactor;
}

var cache = null;
module.exports = {
    getDeviceDetails: function() {
        if(cache) return cache;

        var formFactor = detectFormFactor();
        if (typeof ADLIB_OVERRIDES !== "undefined" && ADLIB_OVERRIDES.formFactor) {
                formFactor = ADLIB_OVERRIDES.formFactor;
        }
        cache = {
            formFactor: formFactor,
            isValidPlatform: isValidPlatform()
        };

        return cache;
    }
};



//NOTE: We are not using platform detection for anything right now as the OfferEngine does automatic device detection,
// but we might need it later, so it's commented out. (comments aren't included in minified build)

// /**
//  * Detects the platform of the device
//  * @returns {string} the platform of the device
//  */
// DeviceDetector.prototype.detectPlatform = function () {
//     if (this.platform) {
//         return this.platform;
//     }
//
//     var platform;
//     if (/Android/i.test_old(this.userAgentString)) {
//         platform = DeviceEnumerations.platform.android;
//     } else if (/iPhone|iPad|iPod/i.test_old(this.userAgentString)) {
//         platform = DeviceEnumerations.platform.iOS;
//     } else {
//         platform = DeviceEnumerations.platform.other;
//     }
//
//
//     this.platform = platform;
//     return this.platform;
// };


},{"../appSettings":8,"../device/enumerations":10}],10:[function(require,module,exports){
/**
 * Created by NiekKruse on 10/9/15.
 *
 * Module contains device related enumerations
 */
var enumerations = {};
enumerations.formFactor = {
    desktop: "desktop",
    app: "app",
    tablet: "tablet",
    smartPhone: "mobile"
};

enumerations.platform = {
    android: "Android",
    iOS: "iOS",
    other: "other"
};

module.exports = enumerations;

},{}],11:[function(require,module,exports){
var utils = require('../utils');
var appSettings = require('../appSettings');

module.exports = {
    getPathname: function () {
        var pathname = window.location.pathname;
        var pathnameArray = pathname.split('/');

        return pathnameArray[pathnameArray.length - 1]; //Return the last file name
    },
    start: function (callback) {
        window[appSettings.globalVarName].init = function (options) {
            callback(options);
        };
    },
    resolveToken: function (callback) {
        //Get the token from the device, or generate a new one
        var token = window.device && window.device.uuid || utils.generateToken();
        callback(token);
    }
};
},{"../appSettings":8,"../utils":18}],12:[function(require,module,exports){
/**
 * Created by NiekKruse on 1/20/16.
 *
 * Module for adding and removing event listeners
 */

/**
 * @enum {string}
 */
var events = {
    afterAdsInserted: "afterAdsInserted"
};
var listeners = {};

/**
 * Check if the event passed is valid
 * @param {string} eventName - Name of the event
 */
function checkEventValid(eventName) {
    if (!events.hasOwnProperty(eventName)) {
        throw eventName + " is not a valid event listener";
    }
}

/**
 * Add a new event listener
 * @param {events} event - The name of the event listener to add an event for
 * @param {function} callback - The callback to invoke when the event is called
 */
function addListener(event, callback) {
    checkEventValid(event);

    if (listeners[event]) {
        listeners[event].push(callback);
    } else {
        listeners[event] = [callback];
    }
}

/**
 * Remove a certain event listener
 * @param {events} event - The name of the event to listen to
 * @param {function} eventHandler - The eventHandler that is bound to this listener and should be removed
 */
function removeListener(event, eventHandler) {
    checkEventValid(event);

    if (listeners[event] && listeners[event].length) {
        var indexOfListener = listeners[event].indexOf(eventHandler);
        if (indexOfListener > -1) {
            listeners[event].splice(indexOfListener, 1);
        }
    }
}

/**
 * Get the event handlers for a certain event
 * @param {events} eventName - The name of the event to get listeners for
 */
function getListeners(eventName) {
    return listeners[eventName];
}

module.exports = {
    events: events,
    addListener: addListener,
    removeListener: removeListener,
    getListeners: getListeners
};
},{}],13:[function(require,module,exports){
/**
 * Main entry point for the ad library.
 */
var deviceDetails = require('./device/deviceDetector').getDeviceDetails(),
    environment = require('./env/environment'),
    AdLib = require('./adLib'),
    appSettings = require("./appSettings");

var isInitialized = false;

window[appSettings.globalVarName] = {ready: false};

if (!isInitialized && deviceDetails.isValidPlatform) {
    environment.start(function(options){
        var adLib = new AdLib(environment, options);
        adLib.init();
    });
    isInitialized = true;
}
},{"./adLib":2,"./appSettings":8,"./device/deviceDetector":9,"./env/environment":11}],14:[function(require,module,exports){
/**
 * Created by NiekKruse on 10/14/15.
 * Module that contains information about the current page
 * @module page
 */
var logger = require("./util/logger"),
    utils = require("./utils"),
    AdContainer = require("./ads/adContainer");

/**
 * Cached version of the token
 * @type {string|null}
 */
var token = null;
var isPreloadingToken = false;

//Keep an array containing callbacks that should fire when the page is ready
var callbacksOnReady = [];

/**
 * Evaluates xPath on a page
 * @param {string} xPathString - The Xpath string to evaluate
 * @returns {Array.<HTMLElement>} - An array of the found HTML elements
 */
function xPath(xPathString) {
    var xResult = document.evaluate(xPathString, document, null, 0, null);
    var xNodes = [];
    var xRes = xResult.iterateNext();
    while (xRes) {
        xNodes.push(xRes);
        xRes = xResult.iterateNext();
    }

    return xNodes;
}


/**
 * Check if the entire page is ready
 * @returns {boolean} - True if the page is ready, false if it isn't.
 */
function isReady() {
    var domReady = document.readyState !== 'loading';
    var tokenReady = token !== null;

    return (domReady && tokenReady);
}

/**
 * Execute all the functions that are waiting for the page to finish loading
 */
function execWaitReadyFunctions() {
    if (isReady()) {
        logger.info('Page is ready. Executing ' + callbacksOnReady.length + ' functions that are waiting.');
        for (var i = 0; i < callbacksOnReady.length; i++) {
            var callback = callbacksOnReady[i];
            callback();
        }
    }
}

function preloadToken (environment) {
    isPreloadingToken = true;
    environment.resolveToken(function (userToken) {
        token = userToken;
        logger.info('User tracking token resolved');
        execWaitReadyFunctions();
    });
}

/**
 * Returns a promise that resolves when the page is ready
 * @param funcToExecute - The function to execute when the page is loaded
 */
function whenReady(funcToExecute) {
    if (isReady()) {
        logger.info('Page is already loaded, instantly executing!');
        funcToExecute();
        return;
    }

    logger.info('Waiting for page to be ready');
    callbacksOnReady.push(funcToExecute);
}

module.exports = {
    /**
     * Check whether the page has responsive design
     * @returns {boolean} indicating whether page is responsive or not
     */
    isResponsive: function () {
        var viewPortMetaTag = document.querySelector("meta[name=viewport]");
        return (viewPortMetaTag !== null);
    },
    /**
     * Gets the adcontainers on the page from the container xPath
     * @param adUnitSettings the settings for the adUnit to get the container of
     * @returns {Array.<Object>} the AdContainer object or null if not found
     */
    getAdContainers: function (adUnitSettings) {
        var containers = adUnitSettings.containers;

        var adContainers = [];

        for (var i = 0; i < containers.length; i++) {
            var container = containers[i];

            var containerXPath = container.xPath;
            var adContainerElements = xPath(containerXPath);

            if (!adContainerElements.length) {
                logger.warn("Ad container with xPath: \"" + containerXPath + "\" could not be found on page");
                continue;
            }

            if (adContainerElements.length > 1) {
                logger.warn("Ad container with xPath:  \"" + containerXPath + "\" has multiple matches");
            }

            adContainers.push(new AdContainer(container, adContainerElements[0]));
        }

        return adContainers;
    },
    /**
     * remove an element from the dom
     * @param domNode the element to remove
     */
    removeElement: function (domNode) {
        domNode.parentElement.removeChild(domNode);
    },
    xPath: xPath,
    /**
     * Get the OfferEngine token
     */
    getToken: function () {
        return token;
    },
    preloadToken: preloadToken,
    addDomReadyListener: function(environment) {
        document.addEventListener('DOMContentLoaded', function () {
            logger.info('DOM is ready');
            if(!token && !isPreloadingToken) {
                logger.info('DOM ready, loading token');
                preloadToken(environment);
                return; //We don't have to check if there's functions waiting, cause the token is only just being preloaded
            }
            execWaitReadyFunctions();
        });
    },
    whenReady: whenReady
};
},{"./ads/adContainer":4,"./util/logger":17,"./utils":18}],15:[function(require,module,exports){
/**
 * Created by NiekKruse on 11/12/15.
 * Utility module containing helper functions for ajax requests
 *
 */
function appendQueryStringOptions(requestUrl, queryStringOptions) {
    requestUrl += "?";
    for (var prop in queryStringOptions) {
        if (queryStringOptions.hasOwnProperty(prop)) {
            requestUrl += prop + "=" + queryStringOptions[prop] + "&";
        }
    }

    //Remove the last & from the string
    requestUrl = requestUrl.substr(0, requestUrl.length - 1);
    return requestUrl;
}

module.exports = {
    /**
     * @callback ajaxSuccessCallback - The callback to invoke when the Ajax call is successful
     * @param {Object} - The data received from the Ajax call
     */

    /**
     * @callback ajaxErrorCallback - The callback to invoke when the Ajax call returns an error
     * @param {Object} - The error object
     */

    /**
     * @typedef {Object} AjaxOptions - The request options
     * @property {string} url - The URL of the get request
     * @property {Object.<string, string>} [query] - The options to append to the query string
     * @property {ajaxSuccessCallback} success - The callback to invoke when the ajax call succeeds
     * @property {ajaxErrorCallback} error - The callback to invoke when the ajax call returns an error
     */

    /**
     * Do a GET request
     * @param {AjaxOptions} options - The options
     */
    get: function (options) {
        var request = new XMLHttpRequest();

        var requestUrl = appendQueryStringOptions(options.url, options.query);
        request.open('get', requestUrl);

        request.onload = function () {
            options.success(JSON.parse(request.responseText));
        };

        request.onerror = function (progressEvent) {
            options.error(progressEvent);
        };

        request.send();
    }
};
},{}],16:[function(require,module,exports){
/**
 * Created by NiekKruse on 10/16/15.
 * Contains app wide enumerations
 */
module.exports = {
    /**
     * The enum for the logLevel
     * @readonly
     * @enum {number}
     */
    logLevel: {
        off: 0,
        debug: 1,
        warn: 2,
        error: 3
    },
    /**
     * The enum for the logType
     * @readonly
     * @enum {string}
     */
    logType: {
        info: "INFO",
        warning: "WARNING",
        error: "ERROR",
        wtf: "FATAL"
    }
};
},{}],17:[function(require,module,exports){
/**
 * Created by NiekKruse on 10/14/15.
 * Helper module for logging purposes
 * @module util/logger
 */
var appSettings = require('../appSettings'),
    enumerations = require('../util/enumerations');

function init() {
    //Check if the logger exists
    if (!window[appSettings.loggerVar]) {
        var Logger = function () {
            this.logs = [];
        };

        Logger.prototype.json = function () {
            return JSON.stringify(this.logs);
        };

        Logger.prototype.print = function () {
            var string = "";

            var consoleRef = console;
            for (var i = 0; i < this.logs.length; i++) {
                var log = this.logs[i];

                consoleRef.log(toFriendlyString(log));
            }

            return string;
        };

        window[appSettings.loggerVar] = new Logger();
    }
}

/**
 * Create a friendly string out of a log entry
 * @param logEntry - The LogEntry to create a friendly string for
 * @returns {string} - the friendly string of the LogEntry
 */
function toFriendlyString(logEntry) {
    return "[PM_Native_Ads " + logEntry.type + "] " + logEntry.time + " - " + logEntry.text;
}

/**
 * Push a logEntry to the array of logs and output it to the console
 * @param logEntry The logEntry to process
 */
function pushLogEntry(logEntry) {
    var logger = window[appSettings.loggerVar];
    if (logger) {
        logger.logs.push(logEntry);

        if (window.console) {
            if (typeof console.error === "function" && typeof console.warn === "function") {
                switch (logEntry.type) {
                    case enumerations.logType.wtf:
                        console.error(toFriendlyString(logEntry));
                        break;
                    case enumerations.logType.error:
                        if (appSettings.logLevel <= enumerations.logLevel.error && appSettings.logLevel > enumerations.logLevel.off) {
                            console.error(toFriendlyString(logEntry));
                        }
                        break;
                    case enumerations.logType.warning:
                        if (appSettings.logLevel <= enumerations.logLevel.warn && appSettings.logLevel > enumerations.logLevel.off) {
                            console.warn(toFriendlyString(logEntry));
                        }
                        break;
                    default:
                        if (appSettings.logLevel <= enumerations.logLevel.debug && appSettings.logLevel > enumerations.logLevel.off) {
                            console.log(toFriendlyString(logEntry));
                            break;
                        }
                }

            } else {
                console.log(toFriendlyString(logEntry));
            }
        }
    }
}

/**
 * Get the current time as a string
 * @returns {string} - the current time in a hh:mm:ss string
 */
function getCurrentTimeString() {
    var today = new Date();
    var hh = today.getHours();
    var mm = today.getMinutes(); //January is 0
    var ss = today.getSeconds();
    var ms = today.getMilliseconds();

    if (hh < 10) {
        hh = '0' + hh;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    if (ss < 10) {
        ss = '0' + ss;
    }

    if (ms < 10) {
        ms = '0' + ms;
    }

    return hh + ":" + mm + ":" + ss + ":" + ms;
}

/**
 * @typedef {Object} LogEntry - A logging entry object
 * @param {string} time - The time of the log as a string
 * @param {text} text - The text of the log
 */

/**
 * Create a new LogEntry object
 * @param {string} logType - the type of log
 * @param {string} logText - The text of the log
 */
function createLogEntry(logType, logText) {
    var logger = window[appSettings.loggerVar];
    if(!logger) {
        init(); //Always initialize on our first log entry
    }

    var log = {
        type: logType,
        time: getCurrentTimeString(),
        text: logText
    };

    pushLogEntry(log);
}


module.exports = {
    /**
     * Creates a new info log
     * @param {string} logText - the text the log should contain
     */
    info: function (logText) {
        createLogEntry(enumerations.logType.info, logText);
    },
    /**
     * Creates a new warning log
     * @param {string} logText - the text the log should contain
     */
    warn: function (logText) {
        createLogEntry(enumerations.logType.warning, logText);
    },
    /**
     * Creates a new error log
     * @param {string} logText - the text the log should contain
     */
    error: function (logText) {
        createLogEntry(enumerations.logType.error, logText);
    },
    /**
     * Creates a new WTF (What a terrible failure) log
     * These should never occur in the application
     * Will always be outputted even if the logLevel is 0
     * @param logText
     */
    wtf: function (logText) {
        createLogEntry(enumerations.logType.wtf, logText);
    }
};

},{"../appSettings":8,"../util/enumerations":16}],18:[function(require,module,exports){
/**
 * Created by NiekKruse on 10/5/15.
 *
 * This module contains Utility functions that can be used throughout the project.
 */

/**
 * Object containing utility functions
 */
var utils = {};

/**
 * Replaces macros in a string with actual values
 * @param {string} strToFormat - The string to format
 * @returns {string} the formatted string
 */
utils.formatString = function (strToFormat) {
    var s = strToFormat;
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }

    return s;
};

/**
 * Converts a string to a string array if the passed parameter is a string.
 * If the passed parameter is already a string array, the function will return it.
 * @param {string|string[]} stringOrArray - The string to convert to an array
 * @returns {Array.<string>} - The string array
 */
utils.stringToArray = function (stringOrArray) {
    if (Array.isArray(stringOrArray))
        return stringOrArray;

    if (typeof stringOrArray === 'string' || stringOrArray instanceof String) {
        //Fix into array
        stringOrArray = [stringOrArray];
    } else {
        throw stringOrArray.toString() + " is not a valid string or string array";
    }

    return stringOrArray;
};

/**
 * Generate a random token for the offerwall
 * TODO: might need some improvement
 * @returns {string} - A unique user token
 */
utils.generateToken = function () {
    var prefix = "offerengine_";
    var now = Date.now();
    var random = Math.random().toString(36).substring(7);
    return prefix + now + random;
};

module.exports = utils;

},{}]},{},[13])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9ncnVudC1icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvYWRDb25maWd1cmF0aW9uL2ZldGNoQ29uZmlndXJhdGlvbi5qcyIsInNyYy9hZExpYi5qcyIsInNyYy9hZHMvYWRCdWlsZGVyLmpzIiwic3JjL2Fkcy9hZENvbnRhaW5lci5qcyIsInNyYy9hZHMvYWRNYW5hZ2VyLmpzIiwic3JjL2Fkcy9mZXRjaEFkcy5qcyIsInNyYy9hZHMvaW5zZXJ0QWRzLmpzIiwic3JjL2FwcFNldHRpbmdzLmpzIiwic3JjL2RldmljZS9kZXZpY2VEZXRlY3Rvci5qcyIsInNyYy9kZXZpY2UvZW51bWVyYXRpb25zLmpzIiwic3JjL2Vudi9lbnZpcm9ubWVudC5qcyIsInNyYy9ldmVudHMuanMiLCJzcmMvbWFpbi5qcyIsInNyYy9wYWdlLmpzIiwic3JjL3V0aWwvYWpheC5qcyIsInNyYy91dGlsL2VudW1lcmF0aW9ucy5qcyIsInNyYy91dGlsL2xvZ2dlci5qcyIsInNyYy91dGlscy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNyREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQy9MQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDeEpBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQy9EQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDM0VBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3JFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbkNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN6RkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3BCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNwRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdkpBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3pEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMzQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzdLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IG5pZWsgb24gMTMvMDQvMTYuXG4gKiBUaGlzIG1vZHVsZSBmZXRjaGVzIHRoZSBhZCBjb25maWd1cmF0aW9uIGZyb20gdGhlIE9mZmVyRW5naW5lIEFQSVxuICovXG52YXIgbG9nZ2VyID0gcmVxdWlyZSgnLi4vdXRpbC9sb2dnZXInKTtcbnZhciBhamF4ID0gcmVxdWlyZSgnLi4vdXRpbC9hamF4Jyk7XG52YXIgYXBwU2V0dGluZ3MgPSByZXF1aXJlKCcuLi9hcHBTZXR0aW5ncycpO1xudmFyIGRldmljZURldGVjdG9yID0gcmVxdWlyZSgnLi4vZGV2aWNlL2RldmljZURldGVjdG9yJyk7XG5cbi8qKlxuICogRmV0Y2ggdGhlIGFkIGNvbmZpZ3VyYXRpb25zIHRvIGxvYWQgb24gdGhlIGN1cnJlbnQgcGFnZSBmcm9tIHRoZSBzZXJ2ZXJcbiAqIEBwYXJhbSBlbnZpcm9ubWVudCAtIFRoZSBlbnZpcm9ubWVudCBzcGVjaWZpYyBpbXBsZW1lbnRhdGlvbnNcbiAqIEBwYXJhbSBhcHBsaWNhdGlvbklkIC0gVGhlIGFwcGxpY2F0aW9uSWQgdG8gZmV0Y2ggYWQgY29uZmlndXJhdGlvbnMgZm9yXG4gKiBAcGFyYW0gY2FsbGJhY2sgLSBUaGUgY2FsbGJhY2sgdG8gZXhlY3V0ZSB3aGVuIHRoZSBjb25maWd1cmF0aW9ucyBhcmUgcmV0cmlldmVkXG4gKi9cbmZ1bmN0aW9uIGZldGNoQ29uZmlndXJhdGlvbihlbnZpcm9ubWVudCwgYXBwbGljYXRpb25JZCwgY2FsbGJhY2spIHtcbiAgICBpZighYXBwbGljYXRpb25JZCkge1xuICAgICAgICBsb2dnZXIuZXJyb3IoJ05vIGFwcGxpY2F0aW9uIElEIHNwZWNpZmllZCcpO1xuICAgICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgLy8gV2UgYWxsb3cgYW4gb3ZlcnJpZGUgdG8gc3BlY2lmeSB0aGUgYWQgY29uZmlndXJhdGlvbnMgZGlyZWN0bHkgaW4gamF2YXNjcmlwdCwgcmF0aGVyIHRoYW5cbiAgICAvLyBIYXZpbmcgdG8gZmV0Y2ggaXQgZnJvbSB0aGUgc2VydmVyXG4gICAgaWYod2luZG93LkFETElCX09WRVJSSURFUyAmJiB3aW5kb3cuQURMSUJfT1ZFUlJJREVTLmFkQ29uZmlndXJhdGlvbnMpIHtcbiAgICAgICAgY2FsbGJhY2sobnVsbCwgd2luZG93LkFETElCX09WRVJSSURFUy5hZENvbmZpZ3VyYXRpb25zKTtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBkZXZpY2VEZXRhaWxzID0gZGV2aWNlRGV0ZWN0b3IuZ2V0RGV2aWNlRGV0YWlscygpO1xuXG4gICAgdmFyIHBhdGhuYW1lID0gZW52aXJvbm1lbnQuZ2V0UGF0aG5hbWUoKTtcbiAgICB2YXIgZm9ybUZhY3RvciA9IGRldmljZURldGFpbHMuZm9ybUZhY3RvcjtcblxuICAgIHZhciByZXF1ZXN0VXJsID0gYXBwU2V0dGluZ3MuY29uZmlndXJhdGlvbnNBcGlVcmw7XG4gICAgdmFyIHF1ZXJ5UGFyYW1zID0ge1xuICAgICAgICBwYXRoOiBwYXRobmFtZSxcbiAgICAgICAgZGV2aWNlOiBmb3JtRmFjdG9yLFxuICAgICAgICBcImFwcGxpY2F0aW9uX2lkXCI6YXBwbGljYXRpb25JZFxuICAgIH07XG5cbiAgICBhamF4LmdldCh7XG4gICAgICAgIHVybDogcmVxdWVzdFVybCxcbiAgICAgICAgcXVlcnk6IHF1ZXJ5UGFyYW1zLFxuICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgY2FsbGJhY2sobnVsbCwgZGF0YSk7XG4gICAgICAgIH0sXG4gICAgICAgIGVycm9yOiBmdW5jdGlvbiAoZXJyKSB7XG4gICAgICAgICAgICBjYWxsYmFjayhlcnIsIG51bGwpO1xuICAgICAgICB9XG4gICAgfSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZmV0Y2hDb25maWd1cmF0aW9uO1xuIiwiLyoqXG4gKiBUaGUgbWFpbiBBZExpYnJhcnkgbW9kdWxlXG4gKi9cbnZhciBhcHBTZXR0aW5ncyA9IHJlcXVpcmUoJy4vYXBwU2V0dGluZ3MnKSxcbiAgICBkZXZpY2VEZXRlY3RvciA9IHJlcXVpcmUoJy4vZGV2aWNlL2RldmljZURldGVjdG9yJyksXG4gICAgbG9nZ2VyID0gcmVxdWlyZSgnLi91dGlsL2xvZ2dlcicpLFxuICAgIHBhZ2UgPSByZXF1aXJlKCcuL3BhZ2UnKSxcbiAgICBBZE1hbmFnZXIgPSByZXF1aXJlKCcuL2Fkcy9hZE1hbmFnZXInKSxcbiAgICB1dGlscyA9IHJlcXVpcmUoJy4vdXRpbHMnKSxcbiAgICBldmVudHMgPSByZXF1aXJlKCcuL2V2ZW50cycpO1xuXG4vKipcbiAqIENvbnN0cnVjdG9yIHRoYXQgY3JlYXRlcyBhbiBpbnN0YW5jZSBvZiB0aGUgQWRMaWJyYXJ5IG9iamVjdFxuICogQHBhcmFtIGVudmlyb25tZW50IC0gVGhlIGVudmlyb25tZW50IG9iamVjdCBjb250YWluaW5nIGVudmlyb25tZW50IHNwZWNpZmljIGZ1bmN0aW9uc1xuICogQHBhcmFtIFtvcHRpb25zXSAtIE9wdGlvbmFsIG9wdGlvbnMgdG8gaW5pdGlhbGl6ZSB0aGUgYWQgbGlicmFyeSB3aXRoXG4gKiBAY29uc3RydWN0b3JcbiAqL1xudmFyIEFkTGliID0gZnVuY3Rpb24gKGVudmlyb25tZW50LCBvcHRpb25zKSB7XG4gICAgdGhpcy5hcHBsaWNhdGlvbklkID0gb3B0aW9ucyAmJiBvcHRpb25zLmFwcGxpY2F0aW9uSWQgfHwgdGhpcy5fZ2V0QXBwbGljYXRpb25JZCgpO1xuXG4gICAgdGhpcy5kZXZpY2VEZXRhaWxzID0gZGV2aWNlRGV0ZWN0b3IuZ2V0RGV2aWNlRGV0YWlscygpO1xuICAgIHRoaXMuZW52aXJvbm1lbnQgPSBlbnZpcm9ubWVudDtcbiAgICB0aGlzLl9hZFByb3ZpZGVyID0gbnVsbDtcbiAgICB0aGlzLnBhZ2UgPSBwYWdlO1xuICAgIHRoaXMuYWRNYW5hZ2VyID0gbnVsbDtcbiAgICB0aGlzLnBhdGggPSB3aW5kb3cubG9jYXRpb24ucGF0aG5hbWU7XG5cblxuICAgIC8vIElmIHRoZSBzY3JpcHQgaW5jbHVkZSBpcyBwbGFjZWQgaW4gdGhlIDxoZWFkPiBvZiB0aGUgcGFnZSxcbiAgICAvLyBkb2N1bWVudC5ib2R5IGlzIG5vdCByZWFkeSB5ZXQsIGFuZCB3ZSBuZWVkIGl0IHRvIHJldHJpZXZlIHRoZSB0b2tlblxuICAgIGlmIChkb2N1bWVudC5ib2R5KSB7XG4gICAgICAgIHBhZ2UucHJlbG9hZFRva2VuKHRoaXMuZW52aXJvbm1lbnQpO1xuICAgIH1cbiAgICBwYWdlLmFkZERvbVJlYWR5TGlzdGVuZXIodGhpcy5lbnZpcm9ubWVudCk7XG5cbiAgICB0aGlzLl9zZXRVcFB1YmxpY0FQSSgpO1xufTtcblxuLyoqXG4gKiBUaGlzIGZ1bmN0aW9uIHNldHMgdXAgdGhlIHB1YmxpYyBBUEkgb24gdGhlIGJyb3dzZXJzJyB3aW5kb3cgb2JqZWN0XG4gKiBzbyB0aGF0IHB1Ymxpc2hlcnMgY2FuIGludGVyYWN0IHdpdGggc29tZSBmdW5jdGlvbnMgb2YgdGhlIEFkIExpYnJhcnkgc2hvdWxkIHRoZXkgbmVlZCB0b1xuICovXG5BZExpYi5wcm90b3R5cGUuX3NldFVwUHVibGljQVBJID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBpbnN0YW5jZSA9IHRoaXM7XG5cbiAgICAvL25vaW5zcGVjdGlvbiBKU1VudXNlZEdsb2JhbFN5bWJvbHNcbiAgICB3aW5kb3dbYXBwU2V0dGluZ3MuZ2xvYmFsVmFyTmFtZV0gPSB7XG4gICAgICAgIHJlYWR5OiB0cnVlLFxuICAgICAgICAvKipcbiAgICAgICAgICogTG9hZCBvbmUgb3IgbW9yZSBhZFVuaXRzIG1hbnVhbGx5XG4gICAgICAgICAqIEBwYXJhbSB7c3RyaW5nW118c3RyaW5nfSBhZFVuaXROYW1lcyAtIFRoZSBuYW1lcyBvZiB0aGUgYWRVbml0cyB0byByZW1vdmVcbiAgICAgICAgICovXG4gICAgICAgIHRyaWdnZXI6IGZ1bmN0aW9uIChhZFVuaXROYW1lcykge1xuICAgICAgICAgICAgYWRVbml0TmFtZXMgPSB1dGlscy5zdHJpbmdUb0FycmF5KGFkVW5pdE5hbWVzKTtcbiAgICAgICAgICAgIGluc3RhbmNlLnRyaWdnZXIoYWRVbml0TmFtZXMpO1xuICAgICAgICB9LFxuICAgICAgICAvKipcbiAgICAgICAgICogUmVmcmVzaCB0aGUgY3VycmVudCBhZHMgb24gdGhlIHBhZ2VcbiAgICAgICAgICovXG4gICAgICAgIHJlZnJlc2g6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGluc3RhbmNlLnJlZnJlc2goKTtcbiAgICAgICAgfSxcbiAgICAgICAgLyoqXG4gICAgICAgICAqIFJlbW92ZSBvbmUgb3IgbW9yZSBhZCB1bml0cyBieSBzcGVjaWZ5aW5nIHRoZWlyIG5hbWVzXG4gICAgICAgICAqIEBwYXJhbSB7c3RyaW5nfHN0cmluZ1tdfSBhZFVuaXROYW1lcyAtIHRoZSBuYW1lKHMpIG9mIHRoZSBhZFVuaXRzIHRvIHJlbW92ZVxuICAgICAgICAgKi9cbiAgICAgICAgcmVtb3ZlQWRVbml0czogZnVuY3Rpb24gKGFkVW5pdE5hbWVzKSB7XG4gICAgICAgICAgICBhZFVuaXROYW1lcyA9IHV0aWxzLnN0cmluZ1RvQXJyYXkoYWRVbml0TmFtZXMpO1xuICAgICAgICAgICAgaW5zdGFuY2UucmVtb3ZlQWRVbml0c0Zyb21QYWdlKGFkVW5pdE5hbWVzKTtcbiAgICAgICAgfSxcbiAgICAgICAgLyoqXG4gICAgICAgICAqIFJlbW92ZSBhbGwgdGhlIGN1cnJlbnQgaW5zZXJ0ZWQgYWRzIGZyb20gdGhlIHBhZ2VcbiAgICAgICAgICovXG4gICAgICAgIHJlbW92ZUFsbEFkczogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaW5zdGFuY2UuYWRNYW5hZ2VyLnJlbW92ZUFsbEFkcygpO1xuICAgICAgICB9LFxuICAgICAgICAvKipcbiAgICAgICAgICogQ29tcGxldGVseSByZWxvYWQgdGhlIGFkIGxpYnJhcnkgYW5kIHJlLXJlYWQgdGhlIGFkIGNvbmZpZ3VyYXRpb25cbiAgICAgICAgICovXG4gICAgICAgIHJlbG9hZDogaW5zdGFuY2UucmVsb2FkLmJpbmQoaW5zdGFuY2UpLFxuICAgICAgICBldmVudHM6IGV2ZW50c1xuICAgIH07XG5cbiAgICB2YXIgbWVzc2FnZURhdGEgPSB7XG4gICAgICAgIHNlbmRlcjogYXBwU2V0dGluZ3MuZ2xvYmFsVmFyTmFtZSxcbiAgICAgICAgbWVzc2FnZTogXCJyZWFkeVwiXG4gICAgfTtcblxuICAgIC8vbm9pbnNwZWN0aW9uIEpTVW5yZXNvbHZlZEZ1bmN0aW9uXG4gICAgd2luZG93LnBvc3RNZXNzYWdlKEpTT04uc3RyaW5naWZ5KG1lc3NhZ2VEYXRhKSwgJyonKTtcbiAgICBsb2dnZXIuaW5mbyhcIlB1YmxpYyBBUEkgaW5pdGlhbGl6ZWQgb24gdmFyICdcIiArIGFwcFNldHRpbmdzLmdsb2JhbFZhck5hbWUgKyBcIidcIik7XG59O1xuXG5BZExpYi5wcm90b3R5cGUuX2dldEFwcGxpY2F0aW9uSWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGFwcGxpY2F0aW9uSWQgPSBudWxsO1xuICAgIGlmICh0eXBlb2Ygd2luZG93LkFETElCX09WRVJSSURFUyAhPT0gXCJ1bmRlZmluZWRcIiAmJiB3aW5kb3cuQURMSUJfT1ZFUlJJREVTLmFwcGxpY2F0aW9uSWQpIHtcbiAgICAgICAgYXBwbGljYXRpb25JZCA9IHdpbmRvdy5BRExJQl9PVkVSUklERVMuYXBwbGljYXRpb25JZDtcbiAgICB9XG4gICAgdmFyIHNjcmlwdFRhZyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJzY3JpcHRbXCIgKyBhcHBTZXR0aW5ncy5hcHBsaWNhdGlvbklkQXR0cmlidXRlICsgXCJdXCIpO1xuICAgIGlmIChzY3JpcHRUYWcpIHtcbiAgICAgICAgYXBwbGljYXRpb25JZCA9IHNjcmlwdFRhZy5nZXRBdHRyaWJ1dGUoYXBwU2V0dGluZ3MuYXBwbGljYXRpb25JZEF0dHJpYnV0ZSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGFwcGxpY2F0aW9uSWQ7XG59O1xuXG4vKipcbiAqIEluaXRpYXRlcyB0aGUgYWQgbGlicmFyeSBieSByZWFkaW5nIHRoZSBjb25maWd1cmF0aW9uIGZpbGUgYW5kIHN0YXJ0aW5nXG4gKiB0aGUgcHJvY2VzcyBvZiBwbGFjaW5nIGFkcyBvbiB0aGUgcGFnZVxuICovXG5BZExpYi5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAoIXRoaXMuYXBwbGljYXRpb25JZCkge1xuICAgICAgICBsb2dnZXIud3RmKCdDb3VsZCBub3QgcmV0cmlldmUgYXBwbGljYXRpb25JZCcpO1xuICAgICAgICByZXR1cm47IC8vRXhpdCB0aGUgYXBwbGljYXRpb25cbiAgICB9XG5cbiAgICB0aGlzLl9pbml0QWRNYW5hZ2VyKHRoaXMuYXBwbGljYXRpb25JZCk7XG59O1xuXG4vKipcbiAqIFJlbW92ZSBhbGwgdGhlIGFkcyBmcm9tIHRoZSBjdXJyZW50IHBhZ2UgYW5kIHJlbG9hZCB0aGUgY29uZmlndXJhdGlvblxuICovXG5BZExpYi5wcm90b3R5cGUucmVsb2FkID0gZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuYWRNYW5hZ2VyLnJlbW92ZUFsbEFkcygpO1xuICAgIHRoaXMuaW5pdCgpO1xufTtcblxuLyoqXG4gKiBDcmVhdGVzIGEgbmV3IGluc3RhbmNlIG9mIHRoZSBhZCBtYW5hZ2VyIGFuZCBsZXRzIHRoZSBtYW5hZ2VyIHN0YXJ0cyBpdHMgaW5zZXJ0aW9uXG4gKiBAcHJpdmF0ZVxuICovXG5BZExpYi5wcm90b3R5cGUuX2luaXRBZE1hbmFnZXIgPSBmdW5jdGlvbiAoYXBwbGljYXRpb25JZCkge1xuICAgIHRoaXMuYWRNYW5hZ2VyID0gbmV3IEFkTWFuYWdlcihhcHBsaWNhdGlvbklkLCB0aGlzLmRldmljZURldGFpbHMsIHRoaXMuZW52aXJvbm1lbnQpO1xuICAgIHRoaXMuYWRNYW5hZ2VyLmxvYWRBZHMoKTtcblxuICAgIHRoaXMuX3N0YXJ0UGF0aENoYW5nZVBvbGxpbmcoKTtcbn07XG5cbi8qKlxuICogU3RhcnRzIHBvbGxpbmcgd2luZG93LmxvY2F0aW9uLnBhdGggZm9yIGNoYW5nZXMgYW5kXG4gKiByZWZyZXNoZXMgdGhlIGFkcyBpZiBhIGNoYW5nZSBpcyBkZXRlY3RlZFxuICogQHByaXZhdGVcbiAqL1xuQWRMaWIucHJvdG90eXBlLl9zdGFydFBhdGhDaGFuZ2VQb2xsaW5nID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBzZWxmID0gdGhpcztcbiAgICBzZXRJbnRlcnZhbChmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh3aW5kb3cubG9jYXRpb24ucGF0aG5hbWUgIT09IHNlbGYucGF0aCkge1xuICAgICAgICAgICAgLy9SZWZyZXNoIHRoZSBBZExpYiBpZiB0aGUgUGF0aG5hbWUgaGFzIGNoYW5nZWRcbiAgICAgICAgICAgIC8vVGhpcyBjYW4gYmUgdmVyeSBjb21tb24gb24gc2luZ2xlIHBhZ2UgYXBwbGljYXRpb25zIHVzaW5nXG4gICAgICAgICAgICAvL1RoZSBicm93c2VyIGhpc3RvcnkgQVBJXG4gICAgICAgICAgICBzZWxmLnBhdGggPSB3aW5kb3cubG9jYXRpb24ucGF0aG5hbWU7XG4gICAgICAgICAgICBzZWxmLnJlZnJlc2goKTtcbiAgICAgICAgfVxuICAgIH0sIDUwMCk7XG59O1xuXG4vKipcbiAqIFJlbW92ZSB0aGUgYWRVbml0cyBzcGVjaWZpZWQgZnJvbSB0aGUgY3VycmVudCBwYWdlXG4gKiBAcGFyYW0ge0FycmF5LjxzdHJpbmc+fSBhZFVuaXROYW1lcyAtIFRoZSBuYW1lcyBvZiB0aGUgYWRVbml0cyB0byByZW1vdmVcbiAqL1xuQWRMaWIucHJvdG90eXBlLnJlbW92ZUFkVW5pdHNGcm9tUGFnZSA9IGZ1bmN0aW9uIChhZFVuaXROYW1lcykge1xuICAgIHRoaXMuYWRNYW5hZ2VyLnJlbW92ZUFkVW5pdHMoYWRVbml0TmFtZXMpO1xufTtcblxuXG4vKipcbiAqIFRoaXMgbWV0aG9kIGNhbiBiZSB1c2VkIGZvciBtYW51YWxseSB0cmlnZ2VyaW5nIGEgcmVsb2FkIG9mIHRoZSBhZHMgb24gdGhlIHBhZ2VcbiAqXG4gKiBGb3IgZXhhbXBsZSwgYSBzaW5nbGUtcGFnZSBhcHBsaWNhdGlvbiBkb2VzIG5vdCByZWxvYWQgdGhlIHNjcmlwdCB3aGVuXG4gKiBjaGFuZ2luZyB0aGUgcGFnZXMuIFRoZSBBZExpYiBBUEkgZXhwb3NlZCBvbiB0aGUgYnJvd3NlcidzIHdpbmRvdyB2YXJpYWJsZVxuICogQ2FuIHVzZSB0aGUgcmVmcmVzaCBmdW5jdGlvbiB0byBtYW51YWxseSB0cmlnZ2VyIGEgcmVsb2FkIG9mIHRoZSBhZHMgb24gYSBwYWdlXG4gKi9cbkFkTGliLnByb3RvdHlwZS5yZWZyZXNoID0gZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuYWRNYW5hZ2VyLnJlZnJlc2goKTtcbn07XG5cbi8qKlxuICogTWFudWFsbHkgdHJpZ2dlciBjZXJ0YWluIGFkIHVuaXRzIHRvIGJlIGluc2VydGVkIG9uIGEgcGFnZVxuICpcbiAqIFRoaXMgaXMgZm9yIGFkVW5pdHMgd2l0aCB0aGUgdHJpZ2dlciBwcm9wZXJ0eSBzcGVjaWZpZWRcbiAqIEZvciBleGFtcGxlLCB3aGVuIGEgZGlhbG9nIGxvYWRzLCBhZHMgaW4gdGhpcyBkaWFsb2cgY2FuIGJlIHNob3duIGJ5IHVzaW5nXG4gKiB0aGUgdHJpZ2dlciBtZXRob2QgZXhwb3NlZCBieSB0aGUgQWRMaWIgQVBJIG9uIHRoZSBicm93c2VyJ3Mgd2luZG93IG9iamVjdFxuICpcbiAqIEBwYXJhbSB7QXJyYXkuPFN0cmluZz59IHVuaXRUcmlnZ2VycyAtIEFycmF5IG9mIHRoZSBhZFVuaXQgbmFtZXMgdG8gdHJpZ2dlclxuICovXG5BZExpYi5wcm90b3R5cGUudHJpZ2dlciA9IGZ1bmN0aW9uICh1bml0VHJpZ2dlcnMpIHtcbiAgICB0aGlzLmFkTWFuYWdlci50cmlnZ2VyKHVuaXRUcmlnZ2Vycyk7XG59O1xuXG5cbm1vZHVsZS5leHBvcnRzID0gQWRMaWI7XG4iLCIvKipcbiAqIENyZWF0ZWQgYnkgTmlla0tydXNlIG9uIDEwLzE1LzE1LlxuICpcbiAqIFRoZSBhZHMgYnVpbGRlciBtb2R1bGUgdGFrZXMgY2FyZSBvZiBidWlsZGluZyB0aGUgSFRNTCBzdHJ1Y3R1cmUgb2YgdGVoIGFkXG4gKi9cbnZhciBsb2dnZXIgPSByZXF1aXJlKCcuLi91dGlsL2xvZ2dlcicpLFxuICAgIGFwcFNldHRpbmdzID0gcmVxdWlyZSgnLi4vYXBwU2V0dGluZ3MnKTtcblxuLyoqXG4gKiBAdHlwZWRlZiB7T2JqZWN0fSBBZE1hY3JvIC0gVGhlIG1hY3JvIG9iamVjdCBkZWZpbmluZyBhZCBtYWNyb3NcbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSBBZE1hY3JvLm1hY3JvIC0gVGhlIG1hY3JvIHRvIHNlYXJjaCBmb3IgaW4gdGhlIHN0cmluZ1xuICogQHByb3BlcnR5IHtzdHJpbmd9IEFkTWFjcm8ucHJvcCAtIFRoZSBwcm9wZXJ0eSBpbiB0aGUgb2JqZWN0IHRvIHJlcGxhY2UgdGhlIHZhbHVlIHdpdGhcbiAqIFRoZSBtYWNyb3MgdG8gcmVwbGFjZSBmb3IgdGhlIGFjdHVhbCBhZHZlcnRpc2VtZW50XG4gKiBAdHlwZSB7QXJyYXkuPEFkTWFjcm8+fVxuICovXG52YXIgYWRNYWNyb3MgPSBbXG4gICAge1xuICAgICAgICBtYWNybzogXCIjI2NhbXBhaWduX25hbWUjI1wiLFxuICAgICAgICBwcm9wOiBcImNhbXBhaWduX25hbWVcIlxuICAgIH0sXG4gICAge1xuICAgICAgICBtYWNybzogXCIjI2NhbXBhaWduX2Rlc2NyaXB0aW9uIyNcIixcbiAgICAgICAgcHJvcDogXCJjYW1wYWlnbl9kZXNjcmlwdGlvblwiXG4gICAgfSxcbiAgICB7XG4gICAgICAgIG1hY3JvOiBcIiMjY2xpY2tfdXJsIyNcIixcbiAgICAgICAgcHJvcDogXCJjbGlja191cmxcIlxuICAgIH0sXG4gICAge1xuICAgICAgICBtYWNybzogXCIjI2NhdGVnb3J5X25hbWUjI1wiLFxuICAgICAgICBwcm9wOiBcImNhdGVnb3J5X25hbWVcIlxuICAgIH0sXG4gICAge1xuICAgICAgICBtYWNybzogXCIjI2NhbXBhaWduX2ltYWdlX3VybCMjXCIsXG4gICAgICAgIHByb3A6IFwiY2FtcGFpZ25faW1hZ2VcIlxuICAgIH1cbl07XG5cbnZhciBjaGlsZFRhZ0RpY3Rpb25hcnkgPSB7XG4gICAgdGFibGU6IFwidGJvZHlcIixcbiAgICB0Ym9keTogXCJ0clwiLFxuICAgIHRoZWFkZXI6IFwidHJcIixcbiAgICB0cjogXCJ0ZFwiXG59O1xuXG4vKipcbiAqIFJlcGxhY2VzIG1hY3JvcyBpbiBhIHN0cmluZyB3aXRoIHRoZSBwcm9wZXJ0aWVzIG9mIGFuIG9iamVjdFxuICogQHBhcmFtIHMgVGhlIHN0cmluZyB0byByZXBsYWNlIHRoZSBtYWNyb3MgaW5cbiAqIEBwYXJhbSBtYWNyb3MgVGhlIG1hY3JvcyB0byByZXBsYWNlIHRoZSBzdHJpbmcgd2l0aFxuICogQHBhcmFtIG9iaiB0aGUgb2JqZWN0IHRvIGdldCB0aGUgbWFjcm8gcHJvcGVydGllcyBmcm9tXG4gKiBAcmV0dXJucyB7U3RyaW5nfSAtIFRoZSBzdHJpbmcgd2l0aCB0aGUgbWFjcm9zIHJlcGxhY2VkXG4gKiBAcHJpdmF0ZVxuICovXG5mdW5jdGlvbiBfcmVwbGFjZU1hY3JvcyhzLCBtYWNyb3MsIG9iaikge1xuICAgIHZhciByZWdleCA9IG51bGw7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBtYWNyb3MubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdmFyIG1hY3JvID0gbWFjcm9zW2ldO1xuICAgICAgICByZWdleCA9IG5ldyBSZWdFeHAobWFjcm8ubWFjcm8sIFwiZ1wiKTtcblxuICAgICAgICBzID0gcy5yZXBsYWNlKHJlZ2V4LCBvYmpbbWFjcm8ucHJvcF0pO1xuICAgIH1cblxuICAgIHJldHVybiBzO1xufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4vL1N0YXJ0IG9mIEFQSVxuLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cblxuLyoqXG4gKiBDcmVhdGVzIGEgbmV3IGluc3RhbmNlIG9mIHRoZSBhZEJ1aWxkZXJcbiAqIEBwYXJhbSBhZHMgYXJyYXkgb2YgYWRzIHByb3ZpZGVkIGJ5IHRoZSBhZHNGZXRjaGVyXG4gKiBAY29uc3RydWN0b3JcbiAqL1xudmFyIEFkQnVpbGRlciA9IGZ1bmN0aW9uIChhZHMpIHtcbiAgICB0aGlzLl9hZHMgPSBhZHM7XG4gICAgdGhpcy5fdWlkID0gMDtcbn07XG5cbi8qKlxuICogUmVwbGFjZXMgdGhlIG1hY3JvcyBmb3IgdGhlIGFjdHVhbCBhZHZlcnRpc2VtZW50XG4gKiBAcGFyYW0ge3N0cmluZ30gaHRtbFN0cmluZyAtIFRoZSBodG1sIHRlbXBsYXRlIHN0cmluZ1xuICogQHBhcmFtIGFkIHRoZSBhZCB0byB1c2UgZm9yIHRoZSBhZFVuaXRcbiAqIEByZXR1cm5zIHtTdHJpbmd9IHRoZSBodG1sU3RyaW5nIHdpdGggcmVwbGFjZWQgbWFjcm9zXG4gKiBAcHJpdmF0ZVxuICovXG5BZEJ1aWxkZXIucHJvdG90eXBlLl9yZXBsYWNlQWRNYWNyb3MgPSBmdW5jdGlvbiAoaHRtbFN0cmluZywgYWQpIHtcbiAgICByZXR1cm4gX3JlcGxhY2VNYWNyb3MoaHRtbFN0cmluZywgYWRNYWNyb3MsIGFkKTtcbn07XG5cbi8qKlxuICogQ3JlYXRlcyBhbiBhY3R1YWwgYWRVbml0IGZyb20gYWRVbml0U2V0dGluZ3NcbiAqIEBwYXJhbSBhZFVuaXRTZXR0aW5ncyB0aGUgc2V0dGluZ3MgZm9yIHRoZSBhZFVuaXRcbiAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IGFkQ29udGFpbmVyRWxlbWVudCAtIFRoZSBIVE1MIGVsZW1lbnQgb2YgdGhlIGFkIGNvbnRhaW5lclxuICogQHJldHVybnMge05vZGV9IHRoZSBIVE1MIE5vZGUgZm9yIHRoZSBhZCB1bml0XG4gKi9cbkFkQnVpbGRlci5wcm90b3R5cGUuY3JlYXRlQWRVbml0ID0gZnVuY3Rpb24gKGFkVW5pdFNldHRpbmdzLCBhZENvbnRhaW5lckVsZW1lbnQpIHtcbiAgICB2YXIgaHRtbFN0cmluZyA9IGFkVW5pdFNldHRpbmdzLmh0bWxUZW1wbGF0ZTtcbiAgICB2YXIgYWQgPSB0aGlzLl9hZHMuc2hpZnQoKTtcblxuICAgIGlmICghYWQpIHtcbiAgICAgICAgLy9XZSByYW4gb3V0IG9mIGFkc1xuICAgICAgICAvL1RPRE86IHdoYXQgZG8gaW4gdGhpcyBjYXNlP1xuICAgICAgICBsb2dnZXIuZXJyb3IoXCJSYW4gb3V0IG9mIGFkcyBiZWZvcmUgYWxsIGFkcyBjb3VsZCBiZSBpbnNlcnRlZFwiKTtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgLy9DcmVhdGUgYSB0ZW1wb3JhcnkgZGl2IHRvIHdyYXAgdGhlIGlubmVyaHRtbCBpbVxuICAgIHZhciB0ZW1wRGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChhZENvbnRhaW5lckVsZW1lbnQudGFnTmFtZSk7XG4gICAgdGVtcERpdi5pbm5lckhUTUwgPSBodG1sU3RyaW5nO1xuXG4gICAgLy9HZXQgdGhlIGh0bWxUZW1wbGF0ZSBzdHJpbmcgYXMgYSBET00gb2JqZWN0XG4gICAgdmFyIGFkRWxlbWVudCA9IHRlbXBEaXYuZmlyc3RDaGlsZDtcbiAgICBhZEVsZW1lbnQuc3R5bGUucG9zaXRpb24gPSBcInJlbGF0aXZlXCI7XG4gICAgYWRFbGVtZW50LmNsYXNzTmFtZSA9IGFwcFNldHRpbmdzLmFkRWxlbWVudENsYXNzbmFtZTtcbiAgICBhZEVsZW1lbnQuaWQgPSB0aGlzLl9uZXdBZEVsZW1lbnRJRCgpO1xuXG4gICAgaHRtbFN0cmluZyA9IHRlbXBEaXYuaW5uZXJIVE1MO1xuICAgIGh0bWxTdHJpbmcgPSB0aGlzLl9yZXBsYWNlQWRNYWNyb3MoaHRtbFN0cmluZywgYWQpO1xuXG4gICAgdGVtcERpdiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoYWRDb250YWluZXJFbGVtZW50LnRhZ05hbWUpO1xuICAgIHRlbXBEaXYuaW5uZXJIVE1MID0gaHRtbFN0cmluZztcblxuICAgIGFkRWxlbWVudCA9IHRlbXBEaXYuZmlyc3RDaGlsZDtcblxuICAgIHJldHVybiBhZEVsZW1lbnQ7XG59O1xuXG4vKipcbiAqIEdlbmVyYXRlcyBhIG5ldyBVbmlxdWUgSUQgZm9yIGFuIGFkIHVuaXRcbiAqIEByZXR1cm5zIHtzdHJpbmd9IHRoZSBuZXcgdW5pcXVlSURcbiAqIEBwcml2YXRlXG4gKi9cbkFkQnVpbGRlci5wcm90b3R5cGUuX25ld0FkRWxlbWVudElEID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBcInBvY2tldF9hZFVuaXRfXCIgKyB0aGlzLl91aWQrKztcbn07XG5cbi8qKlxuICogQ3JlYXRlIGEgbmV3IGNoaWxkIGVsZW1lbnQgZm9yIGEgdGFnIHdpdGggYSBjZXJ0YWluIHRhZyBuYW1lXG4gKiBAcGFyYW0ge0hUTUxFbGVtZW50fSBlbGVtZW50IC0gVGhlIGVsZW1lbnQgdG8gY3JlYXRlIGEgY2hpbGQgZWxlbWVudCBmb3JcbiAqIEByZXR1cm5zIHtIVE1MRWxlbWVudH0gdGhlIGNoaWxkIGVsZW1lbnRcbiAqIEBwcml2YXRlXG4gKi9cbkFkQnVpbGRlci5wcm90b3R5cGUuX2NyZWF0ZUVtcHR5Q2hpbGRFbGVtZW50ID0gZnVuY3Rpb24gKGVsZW1lbnQpIHtcbiAgICB2YXIgZWxlbWVudFRhZ05hbWUgPSBlbGVtZW50LnRhZ05hbWUudG9Mb3dlckNhc2UoKTtcblxuICAgIHZhciB0YWdOYW1lVG9DcmVhdGUgPSBjaGlsZFRhZ0RpY3Rpb25hcnlbZWxlbWVudFRhZ05hbWVdIHx8ICdkaXYnOyAvL1NpbXBseSBjcmVhdGUgYSBkaXYgaXQgaWYgaXQgaXMgbm90IGtub3duIGluIHRoZSBkaWN0aW9uYXJ5XG4gICAgcmV0dXJuIGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQodGFnTmFtZVRvQ3JlYXRlKTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gQWRCdWlsZGVyO1xuIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IE5pZWtLcnVzZSBvbiAxMC8xOS8xNS5cbiAqXG4gKiBNb2R1bGUgdGhhdCBmdW5jdGlvbnMgYXMgYSB3cmFwcGVyIGFyb3VuZCB0aGUgYWQgY29udGFpbmVyIGVsZW1lbnRcbiAqIENvbnRhaW5pbmcgdXNlZnVsIGZ1bmN0aW9ucyBmb3IgZmluZGluZyB0aGUgbmV4dCBwb3NpdGlvbiBpbiBhbiBhZENvbnRhaW5lclxuICovXG52YXIgQWRDb250YWluZXIgPSBmdW5jdGlvbiAoYWRDb250YWluZXJTZXR0aW5ncywgY29udGFpbmVyRWxlbWVudCkge1xuICAgIHRoaXMuY29udGFpbmVyRWxlbWVudCA9IGNvbnRhaW5lckVsZW1lbnQ7XG4gICAgdGhpcy5fY3VycmVudEluZGV4ID0gYWRDb250YWluZXJTZXR0aW5ncy5zdGFydEluZGV4O1xuXG4gICAgdGhpcy5jaGlsZEVsZW1lbnRzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwodGhpcy5jb250YWluZXJFbGVtZW50LmNoaWxkcmVuKTtcbiAgICB2YXIgaW50ZXJ2YWwgPSBhZENvbnRhaW5lclNldHRpbmdzLmludGVydmFsO1xuICAgIGlmICghaW50ZXJ2YWwpIHtcbiAgICAgICAgaW50ZXJ2YWwgPSB0aGlzLl9jYWxjdWxhdGVJbnRlcnZhbChhZENvbnRhaW5lclNldHRpbmdzLm1heE51bWJlck9mQWRzKTtcbiAgICB9XG5cbiAgICB0aGlzLl9zdGFydEluZGV4ID0gYWRDb250YWluZXJTZXR0aW5ncy5zdGFydEluZGV4O1xuICAgIHRoaXMuX2ludGVydmFsID0gaW50ZXJ2YWw7XG59O1xuXG4vKipcbiAqIENhbGN1bGF0ZSB0aGUgaW50ZXJ2YWwgZm9yIGEgdW5pdCB3aGVyZSBvbmx5IGEgbWF4IG51bWJlciBpcyBzZXRcbiAqIEBwYXJhbSBtYXhOdW1iZXJPZkFkcyB0aGUgbWF4IG51bWJlciBvZiBhZHMgdG8gYWQgdG8gdGhlIHBhcmVudCBjb250YWluZXJcbiAqIEBwcml2YXRlXG4gKi9cbkFkQ29udGFpbmVyLnByb3RvdHlwZS5fY2FsY3VsYXRlSW50ZXJ2YWwgPSBmdW5jdGlvbiAobWF4TnVtYmVyT2ZBZHMpIHtcbiAgICB2YXIgZWxlbWVudHMgPSB0aGlzLmNoaWxkRWxlbWVudHMuc2xpY2UodGhpcy5fc3RhcnRJbmRleCAtIDEpO1xuICAgIC8vVE9ETzogbWF5YmUgaW1wcm92ZT9cbiAgICByZXR1cm4gTWF0aC5yb3VuZChlbGVtZW50cy5sZW5ndGggLyBtYXhOdW1iZXJPZkFkcyk7XG59O1xuXG4vKipcbiAqIEdldCB0aGUgbmV4dCBlbGVtZW50IGFmdGVyIHdoaWNoIGFuIGFkIHNob3VsZCBiZSBpbnNlcnRlZFxuICogQHJldHVybnMge05vZGV8bnVsbH0gLSB0aGUgSFRNTCBub2RlIHRvIGluc2VydCBhZnRlciwgb3IgbnVsbCBpZiBpdCBkb2VzIG5vdCBleGlzdFxuICovXG5BZENvbnRhaW5lci5wcm90b3R5cGUuZ2V0TmV4dEVsZW1lbnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKHRoaXMuX2N1cnJlbnRJbmRleCA+IHRoaXMuY2hpbGRFbGVtZW50cy5sZW5ndGggLSAxKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIHZhciBlbGVtZW50ID0gdGhpcy5jaGlsZEVsZW1lbnRzW3RoaXMuX2N1cnJlbnRJbmRleF07XG4gICAgdGhpcy5fY3VycmVudEluZGV4ICs9IHRoaXMuX2ludGVydmFsO1xuICAgIFxuICAgIHJldHVybiBlbGVtZW50O1xufTtcblxuLyoqXG4gKiBnZXQgdGhlIG51bWJlciBvZiBhZHMgdG8gaW5zZXJ0IGluIHRoaXMgYWRDb250YWluZXJcbiAqIEByZXR1cm5zIHtudW1iZXJ9IC0gdGhlIG51bWJlciBvZiBhZHMgdG8gaW5zZXJ0XG4gKi9cbkFkQ29udGFpbmVyLnByb3RvdHlwZS5nZXROdW1iZXJPZkFkc1RvSW5zZXJ0ID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBpbmRleCA9IHRoaXMuX3N0YXJ0SW5kZXg7XG4gICAgdmFyIGNvdW50ZXIgPSAwO1xuXG4gICAgd2hpbGUgKHRoaXMuY2hpbGRFbGVtZW50c1tpbmRleF0pIHtcbiAgICAgICAgY291bnRlcisrO1xuICAgICAgICBpbmRleCArPSB0aGlzLl9pbnRlcnZhbDtcbiAgICB9XG5cbiAgICByZXR1cm4gY291bnRlcjtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gQWRDb250YWluZXI7XG4iLCIvKipcbiAqIENyZWF0ZWQgYnkgTmlla0tydXNlIG9uIDEwLzE2LzE1LlxuICpcbiAqIFRoZSBBZE1hbmFnZXIgbW9kdWxlIHRha2VzIGNhcmUgb2YgYW55dGhpbmcgYWRzIHJlbGF0ZWQgb24gdGhlIHBhZ2UgYW5kIGRpc3RyaWJ1dGVzIHRhc2tzIHRvIHRoZSByaWdodCBtb2R1bGVzXG4gKi9cbnZhciBpbnNlcnRBZHMgPSByZXF1aXJlKCcuL2luc2VydEFkcycpLFxuICAgIGZldGNoQ29uZmlndXJhdGlvbiA9IHJlcXVpcmUoJy4uL2FkQ29uZmlndXJhdGlvbi9mZXRjaENvbmZpZ3VyYXRpb24nKSxcbiAgICBmZXRjaEFkcyA9IHJlcXVpcmUoJy4vZmV0Y2hBZHMnKSxcbiAgICBwYWdlID0gcmVxdWlyZSgnLi4vcGFnZScpLFxuICAgIGxvZ2dlciA9IHJlcXVpcmUoJy4uL3V0aWwvbG9nZ2VyJyksXG4gICAgZXZlbnRzID0gcmVxdWlyZSgnLi4vZXZlbnRzJyk7XG5cbi8qKlxuICogQ3JlYXRlcyBhIG5ldyBpbnN0YW5jZSBvZiB0aGUgYWRNYW5hZ2VyXG4gKiBAcGFyYW0gYXBwbGljYXRpb25JZCAtIFRoZSBJRCBvZiB0aGUgYXBwbGljYXRpb24gdG8gcmVjZWl2ZSBhZHMgZm9yXG4gKiBAcGFyYW0gZGV2aWNlRGV0YWlscyAtIERldGFpbHMgYWJvdXQgdGhlIGN1cnJlbnQgdXNlcnMnIGRldmljZVxuICogQHBhcmFtIGVudmlyb25tZW50IC0gRW52aXJvbm1lbnQgc3BlY2lmaWMgZnVuY3Rpb25zLlxuICogQGNvbnN0cnVjdG9yXG4gKi9cbnZhciBBZE1hbmFnZXIgPSBmdW5jdGlvbiAoYXBwbGljYXRpb25JZCwgZGV2aWNlRGV0YWlscywgZW52aXJvbm1lbnQpIHtcbiAgICB0aGlzLmFwcGxpY2F0aW9uSWQgPSBhcHBsaWNhdGlvbklkO1xuICAgIHRoaXMuZGV2aWNlRGV0YWlscyA9IGRldmljZURldGFpbHM7XG4gICAgdGhpcy5lbnZpcm9ubWVudCA9IGVudmlyb25tZW50O1xuICAgIHRoaXMuX2N1cnJlbnRBZHMgPSBbXTtcbiAgICB0aGlzLl9sb2FkaW5nQWRzID0gW107XG4gICAgdGhpcy5fYWRzV2l0aG91dEltYWdlcyA9IFtdO1xufTtcblxuLyoqXG4gKiBTdGFydHMgdGhlIGFkTWFuYWdlciB0byBkZXRlY3Qgd2hpY2ggYWRzIHNob3VsZCBiZSBpbnNlcnRlZFxuICogaW4gdGhlIGN1cnJlbnQgY29udGV4dCBvZiB0aGUgcGFnZSBhbmQgc3RhcnRzIHRoZSBpbnNlcnRpb25cbiAqIG9mIHRoZXNlIGFkc1xuICovXG5BZE1hbmFnZXIucHJvdG90eXBlLmxvYWRBZHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHRoaXMuX2dldEFkVW5pdHNGb3JDdXJyZW50UGFnZShmdW5jdGlvbiAoYWRVbml0cykge1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGFkVW5pdHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIHZhciBhZFVuaXQgPSBhZFVuaXRzW2ldO1xuICAgICAgICAgICAgc2VsZi5nZXRBZHNGb3JBZFVuaXQoYWRVbml0KTtcbiAgICAgICAgfVxuICAgIH0pO1xufTtcblxuLyoqXG4gKiBSZXRyaWV2ZSBhZHMgZm9yIHRoZSBnaXZlbiBhZCB1bml0XG4gKiBAcGFyYW0gYWRVbml0IC0gVGhlIGFkIHVuaXQgdG8gcmV0cmlldmUgYWRzIGZvclxuICovXG5BZE1hbmFnZXIucHJvdG90eXBlLmdldEFkc0ZvckFkVW5pdCA9IGZ1bmN0aW9uIChhZFVuaXQpIHtcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG5cbiAgICBwYWdlLndoZW5SZWFkeShmdW5jdGlvbiAoKSB7XG4gICAgICAgIGZldGNoQWRzKGFkVW5pdCwgZnVuY3Rpb24gKGFkcykge1xuICAgICAgICAgICAgaWYoYWRzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgICAgIGxvZ2dlci5lcnJvcignTm8gYWRzIHJldHJpZXZlZCBmcm9tIE9mZmVyRW5naW5lJyk7XG4gICAgICAgICAgICAgICAgcmV0dXJuOyAvL0RvIG5vdCBjb250aW51ZS5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgc2VsZi5fb25BZHNMb2FkZWQoYWRVbml0LCBhZHMpO1xuICAgICAgICB9KTtcbiAgICB9KTtcbn07XG5cblxuLyoqXG4gKiBSZW1vdmUgYWxsIHRoZSBjdXJyZW50bHkgaW5zZXJ0ZWQgYWRzXG4gKi9cbkFkTWFuYWdlci5wcm90b3R5cGUucmVtb3ZlQWxsQWRzID0gZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuX3JlbW92ZUluc2VydGVkQWRzKHRoaXMuX2N1cnJlbnRBZHMpO1xuICAgIHRoaXMuX2N1cnJlbnRBZHMgPSBbXTtcbn07XG5cblxuLyoqXG4gKiBNYW51YWxseSB0cmlnZ2VyIHNvbWUgYWRVbml0cyB0byBsb2FkXG4gKiBAcGFyYW0ge3N0cmluZ1tdfSB0cmlnZ2VycyAtIFRoZSB0cmlnZ2VyKHMpIG9mIHRoZSBhZFVuaXRzXG4gKi9cbkFkTWFuYWdlci5wcm90b3R5cGUudHJpZ2dlciA9IGZ1bmN0aW9uICh0cmlnZ2Vycykge1xuICAgIHZhciBhZFVuaXRzID0gdGhpcy5hZENvbmZpZy5nZXRBZFVuaXRzV2l0aFRyaWdnZXIodHJpZ2dlcnMpO1xuXG4gICAgaWYgKGFkVW5pdHMubGVuZ3RoID4gMCkge1xuICAgICAgICB0aGlzLl9sb2FkQWRVbml0cyhhZFVuaXRzKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBsb2dnZXIud2FybihcIk5vIEFkVW5pdHMgZm91bmQgd2l0aCB0cmlnZ2VyKHMpOiBcIiArIHRyaWdnZXJzLmpvaW4oXCIsXCIpKTtcbiAgICB9XG59O1xuXG4vKipcbiAqIFJlbW92ZXMgYWRVbml0cyB3aXRoIGdpdmVuIG5hbWVzIGZyb20gdGhlIHBhZ2VcbiAqIEBwYXJhbSB7c3RyaW5nW119IGFkVW5pdHNUb1JlbW92ZSAtIEFycmF5IGNvbnRhaW5pbmcgdGhlIG5hbWVzIG9mIHRoZSBhZCB1bml0cyB0byByZW1vdmVcbiAqL1xuQWRNYW5hZ2VyLnByb3RvdHlwZS5yZW1vdmVBZFVuaXRzID0gZnVuY3Rpb24gKGFkVW5pdHNUb1JlbW92ZSkge1xuICAgIHZhciBjdXJyZW50QWRzVG9SZW1vdmUgPSB0aGlzLl9jdXJyZW50QWRzLmZpbHRlcihmdW5jdGlvbiAoZGV0YWlscykge1xuICAgICAgICByZXR1cm4gYWRVbml0c1RvUmVtb3ZlLmluZGV4T2YoZGV0YWlscy5hZFVuaXQubmFtZSkgPiAtMTtcbiAgICB9KTtcblxuICAgIHRoaXMuX3JlbW92ZUluc2VydGVkQWRzKGN1cnJlbnRBZHNUb1JlbW92ZSk7XG59O1xuXG4vKipcbiAqIFJlZnJlc2hlcyB0aGUgYWQgbGlicmFyeSBvbiB0aGUgcGFnZVxuICovXG5BZE1hbmFnZXIucHJvdG90eXBlLnJlZnJlc2ggPSBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5yZW1vdmVBbGxBZHMoKTtcbiAgICB0aGlzLmxvYWRBZHMoKTtcbn07XG5cbi8qKlxuICogR2V0IHRoZSBhZCBjb25maWd1cmF0aW9uIGZvciB0aGUgY3VycmVudCBwYWdlXG4gKiBAcHJpdmF0ZVxuICovXG5BZE1hbmFnZXIucHJvdG90eXBlLl9nZXRBZFVuaXRzRm9yQ3VycmVudFBhZ2UgPSBmdW5jdGlvbiAoY2FsbGJhY2spIHtcbiAgICBmZXRjaENvbmZpZ3VyYXRpb24odGhpcy5lbnZpcm9ubWVudCwgdGhpcy5hcHBsaWNhdGlvbklkLCBmdW5jdGlvbiAoZXJyLCBhZFVuaXRzKSB7XG4gICAgICAgIGxvZ2dlci5pbmZvKCdSZWNlaXZlZCAnICsgYWRVbml0cy5sZW5ndGggKyAnIGFkIHVuaXRzIHRvIHJ1biBvbiB0aGUgY3VycmVudCBwYWdlJyk7XG4gICAgICAgIGlmIChlcnIpIHtcbiAgICAgICAgICAgIGxvZ2dlci5lcnJvcignQ291bGQgbm90IGZldGNoIGFkIGNvbmZpZ3VyYXRpb24uJyk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBjYWxsYmFjayhhZFVuaXRzKTtcbiAgICB9KTtcbn07XG5cbi8qKlxuICogUmVtb3ZlIHRoZSBnaXZlbiBpbnNlcnRlZCBhZHMgb24gdGhlIHBhZ2VcbiAqIEBwYXJhbSBjdXJyZW50QWRzIC0gVGhlIGN1cnJlbnQgaW5zZXJ0ZWQgYWRzIHRvIHJlbW92ZVxuICogQHByaXZhdGVcbiAqL1xuQWRNYW5hZ2VyLnByb3RvdHlwZS5fcmVtb3ZlSW5zZXJ0ZWRBZHMgPSBmdW5jdGlvbiAoY3VycmVudEFkcykge1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgY3VycmVudEFkcy5sZW5ndGg7IGkrKykge1xuXG4gICAgICAgIHZhciBjdXJyZW50QWQgPSBjdXJyZW50QWRzW2ldO1xuICAgICAgICBmb3IgKHZhciBqID0gMDsgaiA8IGN1cnJlbnRBZC5hZEVsZW1lbnRzLmxlbmd0aDsgaisrKSB7XG4gICAgICAgICAgICB2YXIgYWRFbGVtZW50VG9SZW1vdmUgPSBjdXJyZW50QWQuYWRFbGVtZW50c1tqXTtcbiAgICAgICAgICAgIGFkRWxlbWVudFRvUmVtb3ZlLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoYWRFbGVtZW50VG9SZW1vdmUpO1xuICAgICAgICB9XG4gICAgfVxufTtcblxuLyoqXG4gKiBDYWxsYmFjayB0aGF0IGdldHMgY2FsbGVkIHdoZW4gdGhlIGFkcyBhcmUgbG9hZGVkIGZyb20gdGhlIEFkUHJvdmlkZXJcbiAqIEBwYXJhbSB7T2JqZWN0fSBhZFVuaXQgLSB0aGUgYWRVbml0IHRvIHdoaWNoIHRoZSBhZHMgYmVsb25nXG4gKiBAcGFyYW0ge1tdfSBhZHMgLSBBcnJheSBvZiBhZHMgb2J0YWluZWQgZnJvbSB0aGUgc2VydmVyXG4gKiBAcHJpdmF0ZVxuICovXG5BZE1hbmFnZXIucHJvdG90eXBlLl9vbkFkc0xvYWRlZCA9IGZ1bmN0aW9uIChhZFVuaXQsIGFkcykge1xuICAgIHZhciBpbnNlcnRlZEFkcyA9IGluc2VydEFkcyhhZFVuaXQsIGFkcywgdGhpcy5fYWRJbWFnZURvbmVMb2FkaW5nLmJpbmQodGhpcykpO1xuICAgIGlmIChpbnNlcnRlZEFkcykge1xuICAgICAgICB0aGlzLl9jdXJyZW50QWRzLnB1c2goe1xuICAgICAgICAgICAgYWRVbml0OiBhZFVuaXQsXG4gICAgICAgICAgICBhZEVsZW1lbnRzOiBpbnNlcnRlZEFkc1xuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLl9sb2FkaW5nQWRzID0gdGhpcy5fbG9hZGluZ0Fkcy5jb25jYXQoaW5zZXJ0ZWRBZHMpO1xuICAgIH1cblxuICAgIHRoaXMuX2NoZWNrQWxsQWRJbWFnZXNEb25lKCk7XG59O1xuXG4vKipcbiAqIENhbGxiYWNrIHRoYXQgaXMgZXhlY3V0ZWQgZWFjaCB0aW1lIHRoZSBpbWFnZSBvZiBhbiBhZCBpcyBkb25lIGxvYWRpbmdcbiAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IGFkRWxlbWVudCAtIFRoZSBlbGVtZW50IHRoYXQgaXMgZG9uZSBsb2FkaW5nXG4gKiBAcGFyYW0ge2Jvb2xlYW59IGhhc0ltYWdlIC0gQm9vbGVhbiBpbmRpY2F0aW5nIHdoZXRoZXIgdGhlIGFkIGNvbnRhaW5lZCBhbiBpbWFnZVxuICogQHByaXZhdGVcbiAqL1xuQWRNYW5hZ2VyLnByb3RvdHlwZS5fYWRJbWFnZURvbmVMb2FkaW5nID0gZnVuY3Rpb24gKGFkRWxlbWVudCwgaGFzSW1hZ2UpIHtcbiAgICBpZiAoIWhhc0ltYWdlKSB7XG4gICAgICAgIHRoaXMuX2Fkc1dpdGhvdXRJbWFnZXMucHVzaChhZEVsZW1lbnQpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHZhciBpbmRleE9mTG9hZGluZ0FkID0gdGhpcy5fbG9hZGluZ0Fkcy5pbmRleE9mKGFkRWxlbWVudCk7XG4gICAgICAgIHRoaXMuX2xvYWRpbmdBZHMuc3BsaWNlKGluZGV4T2ZMb2FkaW5nQWQsIDEpOyAvL1JlbW92ZSBmcm9tIHRoZSBsb2FkaW5nIGFkcyBhcnJheVxuICAgIH1cblxuICAgIHRoaXMuX2NoZWNrQWxsQWRJbWFnZXNEb25lKCk7XG59O1xuXG4vKipcbiAqIENoZWNrcyBpZiBhbGwgYWQgaW1hZ2VzIGFyZSBkb25lIGxvYWRpbmcgYW5kIGVtaXRzIGFuIGV2ZW50IHRoYXQgYWxsIGFkcyBhcmUgcmVhZHlcbiAqIEBwcml2YXRlXG4gKi9cbkFkTWFuYWdlci5wcm90b3R5cGUuX2NoZWNrQWxsQWRJbWFnZXNEb25lID0gZnVuY3Rpb24gKCkge1xuICAgIGlmICgodGhpcy5fYWRzV2l0aG91dEltYWdlcy5sZW5ndGggPT09IDAgJiYgdGhpcy5fbG9hZGluZ0Fkcy5sZW5ndGggPT09IDApIHx8IHRoaXMuX2xvYWRpbmdBZHMubGVuZ3RoID09PSB0aGlzLl9hZHNXaXRob3V0SW1hZ2VzLmxlbmd0aCkge1xuICAgICAgICBsb2dnZXIuaW5mbyhcIkFsbCBhZHMgYW5kIGltYWdlcyBhcmUgZG9uZSBsb2FkaW5nXCIpO1xuICAgICAgICB2YXIgZXZlbnRMaXN0ZW5lcnMgPSBldmVudHMuZ2V0TGlzdGVuZXJzKGV2ZW50cy5ldmVudHMuYWZ0ZXJBZHNJbnNlcnRlZCk7XG4gICAgICAgIGlmIChldmVudExpc3RlbmVycyAmJiBldmVudExpc3RlbmVycy5sZW5ndGgpIHtcbiAgICAgICAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgZXZlbnRMaXN0ZW5lcnMubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgICAgICAgICBldmVudExpc3RlbmVyc1tqXSh0aGlzLl9jdXJyZW50QWRzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn07XG5cblxubW9kdWxlLmV4cG9ydHMgPSBBZE1hbmFnZXI7IiwiLyoqXG4gKiBDcmVhdGVkIGJ5IG5pZWsgb24gMTMvMDQvMTYuXG4gKlxuICogVGhpcyBtb2R1bGUgcHJvdmlkZXMgYWRzIHRvIHRoZSBsaWJyYXJ5XG4gKi9cbnZhciBhamF4ID0gcmVxdWlyZSgnLi4vdXRpbC9hamF4Jyk7XG52YXIgcGFnZSA9IHJlcXVpcmUoJy4uL3BhZ2UnKTtcbnZhciBsb2dnZXIgPSByZXF1aXJlKCcuLi91dGlsL2xvZ2dlcicpO1xudmFyIGFwcFNldHRpbmdzID0gcmVxdWlyZSgnLi4vYXBwU2V0dGluZ3MnKTtcblxuLyoqXG4gKiBHZXQgdGhlIG51bWJlciBvZiBhZHMgdGhpcyB1bml0IG5lZWRzIHRvIHBsYWNlIGFsbCB0aGUgYWRzIG9uIHRoZSBwYWdlXG4gKiBAcGFyYW0gYWRVbml0IFRoZSBhZCB1bml0IHRvIGdldCB0aGUgcmVxdWlyZWQgbnVtYmVyIG9mIGFkcyBmb3JcbiAqIEByZXR1cm5zIHtudW1iZXJ9IHRoZSBudW1iZXIgb2YgcmVxdWlyZWQgYWRzXG4gKiBAcHJpdmF0ZVxuICovXG5mdW5jdGlvbiBfZ2V0UmVxdWlyZWRBZENvdW50Rm9yQWRVbml0KGFkVW5pdCkge1xuICAgIHZhciBhZENvbnRhaW5lcnMgPSBwYWdlLmdldEFkQ29udGFpbmVycyhhZFVuaXQpO1xuXG4gICAgaWYgKCFhZENvbnRhaW5lcnMubGVuZ3RoKSB7XG4gICAgICAgIHJldHVybiAwO1xuICAgIH1cblxuICAgIHZhciBudW1iZXJPZkFkc1RvSW5zZXJ0ID0gMDtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGFkQ29udGFpbmVycy5sZW5ndGg7IGkrKykge1xuICAgICAgICB2YXIgYWRDb250YWluZXIgPSBhZENvbnRhaW5lcnNbaV07XG4gICAgICAgIG51bWJlck9mQWRzVG9JbnNlcnQgKz0gYWRDb250YWluZXIuZ2V0TnVtYmVyT2ZBZHNUb0luc2VydCgpO1xuICAgIH1cblxuICAgIHJldHVybiBudW1iZXJPZkFkc1RvSW5zZXJ0O1xufVxuXG4vKipcbiAqIFJlcXVlc3QgYWRzIGZyb20gdGhlIG9mZmVyRW5naW5lXG4gKiBAcGFyYW0gYWRVbml0IC0gVGhlIGFkIFVuaXQgdGhhdCBpcyByZXF1ZXN0aW5nIGFkc1xuICogQHBhcmFtIGNhbGxiYWNrIC0gVGhlIGNhbGxiYWNrIHRvIGV4ZWN1dGUgY29udGFpbmluZyB0aGUgYWRzXG4gKi9cbmZ1bmN0aW9uIHJlcXVlc3RBZHMoYWRVbml0LCBjYWxsYmFjaykge1xuICAgIHZhciBsaW1pdCA9IF9nZXRSZXF1aXJlZEFkQ291bnRGb3JBZFVuaXQoYWRVbml0KTtcbiAgICB2YXIgdG9rZW4gPSBwYWdlLmdldFRva2VuKCk7XG5cbiAgICB2YXIgcmVxdWVzdFF1ZXJ5ID0ge1xuICAgICAgICBcIm91dHB1dFwiOiBcImpzb25cIixcbiAgICAgICAgXCJwbGFjZW1lbnRfa2V5XCI6IGFkVW5pdC5wbGFjZW1lbnRLZXksXG4gICAgICAgIFwibGltaXRcIjogbGltaXQsXG4gICAgICAgIFwidG9rZW5cIjogdG9rZW4sXG4gICAgICAgIFwiYXV0b19kZXZpY2VcIjogMVxuICAgIH07XG5cbiAgICAvL25vaW5zcGVjdGlvbiBKU1VucmVzb2x2ZWRWYXJpYWJsZVxuICAgIGlmICh0eXBlb2YgQURMSUJfT1ZFUlJJREVTICE9PSBcInVuZGVmaW5lZFwiICYmIEFETElCX09WRVJSSURFUy5mb3JtRmFjdG9yKSB7XG4gICAgICAgIGlmIChBRExJQl9PVkVSUklERVMucGxhdGZvcm0gJiYgQURMSUJfT1ZFUlJJREVTLmZ1bGxEZXZpY2VOYW1lICYmIEFETElCX09WRVJSSURFUy52ZXJzaW9uKSB7XG4gICAgICAgICAgICBkZWxldGUgcmVxdWVzdFF1ZXJ5LmF1dG9fZGV2aWNlO1xuICAgICAgICAgICAgcmVxdWVzdFF1ZXJ5Lm9zID0gQURMSUJfT1ZFUlJJREVTLnBsYXRmb3JtO1xuICAgICAgICAgICAgcmVxdWVzdFF1ZXJ5Lm1vZGVsID0gQURMSUJfT1ZFUlJJREVTLmZ1bGxEZXZpY2VOYW1lO1xuICAgICAgICAgICAgcmVxdWVzdFF1ZXJ5LnZlcnNpb24gPSBBRExJQl9PVkVSUklERVMudmVyc2lvbjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGFqYXguZ2V0KHtcbiAgICAgICAgdXJsOiBhcHBTZXR0aW5ncy5hZEFwaUJhc2VVcmwsXG4gICAgICAgIHF1ZXJ5OiByZXF1ZXN0UXVlcnksXG4gICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICBpZiAoZGF0YS5sZW5ndGggIT09IGxpbWl0KSB7XG4gICAgICAgICAgICAgICAgbG9nZ2VyLndhcm4oXCJUcmllZCB0byBmZXRjaCBcIiArIGxpbWl0ICsgXCIgYWRzLCBidXQgb25seSByZWNlaXZlZCBcIiArIGRhdGEubGVuZ3RoKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY2FsbGJhY2soZGF0YSk7XG4gICAgICAgIH0sXG4gICAgICAgIGVycm9yOiBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgbG9nZ2VyLnd0ZignQW4gZXJyb3Igb2NjdXJyZWQgdHJ5aW5nIHRvIGZldGNoIGFkcycpO1xuICAgICAgICB9XG4gICAgfSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gcmVxdWVzdEFkczsiLCIvKipcbiAqIENyZWF0ZWQgYnkgbmllayBvbiAxMy8wNC8xNi5cbiAqIFRoaXMgbW9kdWxlIHRha2VzIGNhcmUgb2YgdGhlIGFkIGluc2VydGlvbiBmb3IgYSBnaXZlbiBhZCB1bml0XG4gKi9cbnZhciBwYWdlID0gcmVxdWlyZSgnLi4vcGFnZScpO1xudmFyIEFkQnVpbGRlciA9IHJlcXVpcmUoJy4vYWRCdWlsZGVyJyk7XG52YXIgZGV2aWNlRGV0ZWN0b3IgPSByZXF1aXJlKCcuLi9kZXZpY2UvZGV2aWNlRGV0ZWN0b3InKTtcbnZhciBsb2dnZXIgPSByZXF1aXJlKCcuLi91dGlsL2xvZ2dlcicpO1xuXG4vKipcbiAqIEluc2VydCBhZHZlcnRpc2VtZW50cyBmb3IgdGhlIGdpdmVuIGFkIHVuaXQgb24gdGhlIHBhZ2VcbiAqIEBwYXJhbSBhZFVuaXQgLSBUaGUgYWQgdW5pdCB0byBpbnNlcnQgYWR2ZXJ0aXNlbWVudHMgZm9yXG4gKiBAcGFyYW0gYWRzIC0gQXJyYXkgb2YgYWRzIHJldHJpZXZlZCBmcm9tIE9mZmVyRW5naW5lXG4gKiBAcGFyYW0gYWRMb2FkZWRDYWxsYmFjayAtIENhbGxiYWNrIHRvIGV4ZWN1dGUgd2hlbiB0aGUgYWRzIGFyZSBmdWxseSBsb2FkZWRcbiAqIEByZXR1cm5zIHtBcnJheX1cbiAqL1xuZnVuY3Rpb24gaW5zZXJ0QWRzKGFkVW5pdCwgYWRzLCBhZExvYWRlZENhbGxiYWNrKSB7XG4gICAgdmFyIGFkQ29udGFpbmVycyA9IHBhZ2UuZ2V0QWRDb250YWluZXJzKGFkVW5pdCk7XG5cbiAgICBpZiAoIWFkQ29udGFpbmVycy5sZW5ndGgpIHtcbiAgICAgICAgbG9nZ2VyLmVycm9yKFwiTm8gYWQgY29udGFpbmVycyBjb3VsZCBiZSBmb3VuZC4gc3RvcHBpbmcgaW5zZXJ0aW9uIGZvciBhZFVuaXQgXCIgKyBhZFVuaXQubmFtZSk7XG4gICAgICAgIHJldHVybiBbXTsgLy9BZCBjYW4ndCBiZSBpbnNlcnRlZFxuICAgIH1cbiAgICBcbiAgICB2YXIgYWRCdWlsZGVyID0gbmV3IEFkQnVpbGRlcihhZHMpO1xuXG4gICAgdmFyIGJlZm9yZUVsZW1lbnQ7XG4gICAgdmFyIGluc2VydGVkQWRFbGVtZW50cyA9IFtdO1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgYWRDb250YWluZXJzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHZhciBhZENvbnRhaW5lciA9IGFkQ29udGFpbmVyc1tpXTtcbiAgICAgICAgd2hpbGUgKChiZWZvcmVFbGVtZW50ID0gYWRDb250YWluZXIuZ2V0TmV4dEVsZW1lbnQoKSkgIT09IG51bGwpIHtcbiAgICAgICAgICAgIHZhciBhZFRvSW5zZXJ0ID0gYWRCdWlsZGVyLmNyZWF0ZUFkVW5pdChhZFVuaXQsIGFkQ29udGFpbmVyLmNvbnRhaW5lckVsZW1lbnQpO1xuXG4gICAgICAgICAgICBpZiAoYWRUb0luc2VydCA9PT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIC8vd2UgcmFuIG91dCBvZiBhZHMuXG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGluc2VydGVkQWRFbGVtZW50cy5wdXNoKGFkVG9JbnNlcnQpO1xuICAgICAgICAgICAgYmVmb3JlRWxlbWVudC5wYXJlbnROb2RlLmluc2VydEJlZm9yZShhZFRvSW5zZXJ0LCBiZWZvcmVFbGVtZW50Lm5leHRTaWJsaW5nKTtcblxuICAgICAgICAgICAgLy8gdmFyIGVsZW1lbnREaXNwbGF5ID0gYWRUb0luc2VydC5zdHlsZS5kaXNwbGF5IHx8IFwiYmxvY2tcIjtcbiAgICAgICAgICAgIC8vVE9ETzogV2h5IGFyZSB3ZSBkZWZhdWx0aW5nIHRvIGJsb2NrIGhlcmU/XG4gICAgICAgICAgICAvLyBhZFRvSW5zZXJ0LnN0eWxlLmRpc3BsYXkgPSBlbGVtZW50RGlzcGxheTtcbiAgICAgICAgICAgIGhhbmRsZUltYWdlTG9hZChhZFRvSW5zZXJ0LCBhZExvYWRlZENhbGxiYWNrKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBpbnNlcnRlZEFkRWxlbWVudHM7XG59XG5cbi8qKlxuICogQWRkIGFuIGV2ZW50IGhhbmRsZXIgdG8gdGhlIG9ubG9hZCBvZiBhZCBpbWFnZXMuXG4gKiBAcGFyYW0gYWRFbGVtZW50IC0gVGhlIEhUTUwgZWxlbWVudCBvZiB0aGUgYWR2ZXJ0aXNlbWVudFxuICogQHBhcmFtIGFkTG9hZGVkQ2FsbGJhY2sgLSBDYWxsYmFjayB0byBleGVjdXRlIHdoZW4gYWRzIGFyZSBsb2FkZWRcbiAqL1xuZnVuY3Rpb24gaGFuZGxlSW1hZ2VMb2FkKGFkRWxlbWVudCwgYWRMb2FkZWRDYWxsYmFjaykge1xuICAgIHZhciBhZEltYWdlID0gYWRFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoXCJpbWdcIik7XG4gICAgaWYgKGFkSW1hZ2UpIHtcbiAgICAgICAgKGZ1bmN0aW9uIChhZFRvSW5zZXJ0LCBhZEltYWdlKSB7XG4gICAgICAgICAgICBhZEltYWdlLm9ubG9hZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBhZExvYWRlZENhbGxiYWNrKGFkVG9JbnNlcnQsIHRydWUpO1xuICAgICAgICAgICAgfTtcbiAgICAgICAgfSkoYWRFbGVtZW50LCBhZEltYWdlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBhZExvYWRlZENhbGxiYWNrKGFkRWxlbWVudCwgZmFsc2UpO1xuICAgIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBpbnNlcnRBZHM7IiwiLyoqXG4gKiBDcmVhdGVkIGJ5IE5pZWtLcnVzZSBvbiAxMC81LzE1LlxuICogVGhpcyBtb2R1bGUgY29udGFpbnMgbGlicmFyeSB3aWRlIGRlYnVnIHNldHRpbmdzXG4gKiBBIG1vZHVsZSB0aGF0IGV4cG9zZXMgdGhlIGFwcCBzZXR0aW5nc1xuICovXG52YXIgZW51bWVyYXRpb25zID0gcmVxdWlyZShcIi4vdXRpbC9lbnVtZXJhdGlvbnNcIik7XG5cbi8qKlxuICogRXhwb3J0cyB0aGUgYXBwU2V0dGluZ3NcbiAqL1xubW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgY29uZmlnRmlsZU5hbWU6IFwiL2FkY29uZmlnLmpzb25cIixcbiAgICBpc0RlYnVnOiBmYWxzZSxcbiAgICBsb2dMZXZlbDogZW51bWVyYXRpb25zLmxvZ0xldmVsLmRlYnVnLFxuICAgIGFkQXBpQmFzZVVybDogXCJodHRwOi8vb2ZmZXJ3YWxsLjEydHJhY2t3YXkuY29tL293LnBocFwiLFxuICAgIGdsb2JhbFZhck5hbWU6IFwicG9ja2V0X25hdGl2ZV9hZHNcIixcbiAgICB4RG9tYWluU3RvcmFnZVVSTDogXCIgaHR0cDovL29mZmVyd2FsbC4xMnRyYWNrd2F5LmNvbS94RG9tYWluU3RvcmFnZS5odG1sXCIsXG4gICAgdG9rZW5Db29raWVLZXk6IFwicG1fb2ZmZXJ3YWxsXCIsXG4gICAgbG9nZ2VyVmFyOiBcIl9fYWRsaWJMb2dcIixcbiAgICBkZWZhdWx0U21hcnRQaG9uZVdpZHRoOiAzNzUsXG4gICAgZGVmYXVsdFRhYmxldFdpZHRoOiA3NjgsXG4gICAgY29uZmlndXJhdGlvbnNBcGlVcmw6ICdodHRwOi8vb2ZmZXJ3YWxsLjEydHJhY2t3YXkuY29tL293LnBocCcsXG4gICAgYXBwbGljYXRpb25JZEF0dHJpYnV0ZTogJ2RhdGEtYXBwbGljYXRpb24taWQnLFxuICAgIGFkRWxlbWVudENsYXNzbmFtZTogJ3BtX25hdGl2ZV9hZF91bml0JyxcbiAgICBkaXNwbGF5U2V0dGluZ3M6IHtcbiAgICAgICAgbW9iaWxlOiB7XG4gICAgICAgICAgICBtaW5XaWR0aDogMCxcbiAgICAgICAgICAgIG1heFdpZHRoOiA0MTVcbiAgICAgICAgfSxcbiAgICAgICAgdGFibGV0OiB7XG4gICAgICAgICAgICBtaW5XaWR0aDogNDE1LFxuICAgICAgICAgICAgbWF4V2lkdGg6IDEwMjRcbiAgICAgICAgfVxuICAgIH1cbn07XG4iLCIvKipcbiAqIENyZWF0ZWQgYnkgTmlla0tydXNlIG9uIDEwLzkvMTUuXG4gKlxuICogVGhpcyBtb2R1bGUgY29udGFpbnMgZnVuY3Rpb25hbGl0eSBmb3IgZGV0ZWN0aW5nIGRldGFpbHMgYWJvdXQgYSBkZXZpY2VcbiAqL1xudmFyIGFwcFNldHRpbmdzID0gcmVxdWlyZSgnLi4vYXBwU2V0dGluZ3MnKSxcbiAgICBEZXZpY2VFbnVtZXJhdGlvbnMgPSByZXF1aXJlKCcuLi9kZXZpY2UvZW51bWVyYXRpb25zJyk7XG5cbi8qKlxuICogQ2hlY2sgaWYgdGhlIHBsYXRmb3JtIHRoZSB1c2VyIGlzIGN1cnJlbnRseSB2aXNpdGluZyB0aGUgcGFnZSB3aXRoIGlzIHZhbGlkIGZvclxuICogdGhlIGFkIGxpYnJhcnkgdG8gcnVuXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAqL1xuZnVuY3Rpb24gaXNWYWxpZFBsYXRmb3JtKCkge1xuICAgIGlmICh0eXBlb2YgQURMSUJfT1ZFUlJJREVTICE9PSBcInVuZGVmaW5lZFwiICYmIEFETElCX09WRVJSSURFUy5wbGF0Zm9ybSkge1xuICAgICAgICByZXR1cm4gdHJ1ZTsgLy9JZiBhIHBsYXRmb3JtIG92ZXJyaWRlIGlzIHNldCwgaXQncyBhbHdheXMgdmFsaWRcbiAgICB9XG4gICAgcmV0dXJuIC9pUGhvbmV8aVBhZHxpUG9kfEFuZHJvaWQvaS50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQpO1xufVxuXG4vKipcbiAqIERldGVjdHMgdGhlIGRldmljZSBmb3JtIGZhY3RvciBiYXNlZCBvbiB0aGUgVmlld1BvcnQgYW5kIHRoZSBkZXZpY2VXaWR0aHMgaW4gQXBwU2V0dGluZ3NcbiAqIFRoZSB0ZXN0X29sZCBpcyBkb25lIGJhc2VkIG9uIHRoZSB2aWV3cG9ydCwgYmVjYXVzZSBpdCBpcyBhbHJlYWR5IHZhbGlkYXRlZCB0aGF0IGEgZGV2aWNlIGlzIEFuZHJvaWQgb3IgaU9TXG4gKiBAcmV0dXJucyB7Kn0gdGhlIGZvcm0gZmFjdG9yIG9mIHRoZSBkZXZpY2VcbiAqIEBwcml2YXRlXG4gKi9cbmZ1bmN0aW9uIGRldGVjdEZvcm1GYWN0b3IoKSB7XG4gICAgdmFyIHZpZXdQb3J0V2lkdGggPSBNYXRoLm1heChkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50V2lkdGgsIHdpbmRvdy5pbm5lcldpZHRoIHx8IDApO1xuXG4gICAgdmFyIGRpc3BsYXlTZXR0aW5ncyA9IGFwcFNldHRpbmdzLmRpc3BsYXlTZXR0aW5nczsgLy9jb252ZW5pZW5jZSB2YXJpYWJsZVxuICAgIHZhciBmb3JtRmFjdG9yO1xuXG4gICAgaWYgKHZpZXdQb3J0V2lkdGggPj0gZGlzcGxheVNldHRpbmdzLm1vYmlsZS5taW5XaWR0aCAmJiB2aWV3UG9ydFdpZHRoIDw9IGRpc3BsYXlTZXR0aW5ncy5tb2JpbGUubWF4V2lkdGgpIHtcbiAgICAgICAgZm9ybUZhY3RvciA9IERldmljZUVudW1lcmF0aW9ucy5mb3JtRmFjdG9yLnNtYXJ0UGhvbmU7XG4gICAgfSBlbHNlIGlmICh2aWV3UG9ydFdpZHRoID49IGRpc3BsYXlTZXR0aW5ncy50YWJsZXQubWluV2lkdGggJiYgdmlld1BvcnRXaWR0aCA8PSBkaXNwbGF5U2V0dGluZ3MudGFibGV0Lm1heFdpZHRoKSB7XG4gICAgICAgIGZvcm1GYWN0b3IgPSBEZXZpY2VFbnVtZXJhdGlvbnMuZm9ybUZhY3Rvci50YWJsZXQ7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgZm9ybUZhY3RvciA9IERldmljZUVudW1lcmF0aW9ucy5mb3JtRmFjdG9yLmRlc2t0b3A7XG4gICAgfVxuXG4gICAgcmV0dXJuIGZvcm1GYWN0b3I7XG59XG5cbnZhciBjYWNoZSA9IG51bGw7XG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgICBnZXREZXZpY2VEZXRhaWxzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgaWYoY2FjaGUpIHJldHVybiBjYWNoZTtcblxuICAgICAgICB2YXIgZm9ybUZhY3RvciA9IGRldGVjdEZvcm1GYWN0b3IoKTtcbiAgICAgICAgaWYgKHR5cGVvZiBBRExJQl9PVkVSUklERVMgIT09IFwidW5kZWZpbmVkXCIgJiYgQURMSUJfT1ZFUlJJREVTLmZvcm1GYWN0b3IpIHtcbiAgICAgICAgICAgICAgICBmb3JtRmFjdG9yID0gQURMSUJfT1ZFUlJJREVTLmZvcm1GYWN0b3I7XG4gICAgICAgIH1cbiAgICAgICAgY2FjaGUgPSB7XG4gICAgICAgICAgICBmb3JtRmFjdG9yOiBmb3JtRmFjdG9yLFxuICAgICAgICAgICAgaXNWYWxpZFBsYXRmb3JtOiBpc1ZhbGlkUGxhdGZvcm0oKVxuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiBjYWNoZTtcbiAgICB9XG59O1xuXG5cblxuLy9OT1RFOiBXZSBhcmUgbm90IHVzaW5nIHBsYXRmb3JtIGRldGVjdGlvbiBmb3IgYW55dGhpbmcgcmlnaHQgbm93IGFzIHRoZSBPZmZlckVuZ2luZSBkb2VzIGF1dG9tYXRpYyBkZXZpY2UgZGV0ZWN0aW9uLFxuLy8gYnV0IHdlIG1pZ2h0IG5lZWQgaXQgbGF0ZXIsIHNvIGl0J3MgY29tbWVudGVkIG91dC4gKGNvbW1lbnRzIGFyZW4ndCBpbmNsdWRlZCBpbiBtaW5pZmllZCBidWlsZClcblxuLy8gLyoqXG4vLyAgKiBEZXRlY3RzIHRoZSBwbGF0Zm9ybSBvZiB0aGUgZGV2aWNlXG4vLyAgKiBAcmV0dXJucyB7c3RyaW5nfSB0aGUgcGxhdGZvcm0gb2YgdGhlIGRldmljZVxuLy8gICovXG4vLyBEZXZpY2VEZXRlY3Rvci5wcm90b3R5cGUuZGV0ZWN0UGxhdGZvcm0gPSBmdW5jdGlvbiAoKSB7XG4vLyAgICAgaWYgKHRoaXMucGxhdGZvcm0pIHtcbi8vICAgICAgICAgcmV0dXJuIHRoaXMucGxhdGZvcm07XG4vLyAgICAgfVxuLy9cbi8vICAgICB2YXIgcGxhdGZvcm07XG4vLyAgICAgaWYgKC9BbmRyb2lkL2kudGVzdF9vbGQodGhpcy51c2VyQWdlbnRTdHJpbmcpKSB7XG4vLyAgICAgICAgIHBsYXRmb3JtID0gRGV2aWNlRW51bWVyYXRpb25zLnBsYXRmb3JtLmFuZHJvaWQ7XG4vLyAgICAgfSBlbHNlIGlmICgvaVBob25lfGlQYWR8aVBvZC9pLnRlc3Rfb2xkKHRoaXMudXNlckFnZW50U3RyaW5nKSkge1xuLy8gICAgICAgICBwbGF0Zm9ybSA9IERldmljZUVudW1lcmF0aW9ucy5wbGF0Zm9ybS5pT1M7XG4vLyAgICAgfSBlbHNlIHtcbi8vICAgICAgICAgcGxhdGZvcm0gPSBEZXZpY2VFbnVtZXJhdGlvbnMucGxhdGZvcm0ub3RoZXI7XG4vLyAgICAgfVxuLy9cbi8vXG4vLyAgICAgdGhpcy5wbGF0Zm9ybSA9IHBsYXRmb3JtO1xuLy8gICAgIHJldHVybiB0aGlzLnBsYXRmb3JtO1xuLy8gfTtcblxuIiwiLyoqXG4gKiBDcmVhdGVkIGJ5IE5pZWtLcnVzZSBvbiAxMC85LzE1LlxuICpcbiAqIE1vZHVsZSBjb250YWlucyBkZXZpY2UgcmVsYXRlZCBlbnVtZXJhdGlvbnNcbiAqL1xudmFyIGVudW1lcmF0aW9ucyA9IHt9O1xuZW51bWVyYXRpb25zLmZvcm1GYWN0b3IgPSB7XG4gICAgZGVza3RvcDogXCJkZXNrdG9wXCIsXG4gICAgYXBwOiBcImFwcFwiLFxuICAgIHRhYmxldDogXCJ0YWJsZXRcIixcbiAgICBzbWFydFBob25lOiBcIm1vYmlsZVwiXG59O1xuXG5lbnVtZXJhdGlvbnMucGxhdGZvcm0gPSB7XG4gICAgYW5kcm9pZDogXCJBbmRyb2lkXCIsXG4gICAgaU9TOiBcImlPU1wiLFxuICAgIG90aGVyOiBcIm90aGVyXCJcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gZW51bWVyYXRpb25zO1xuIiwidmFyIHV0aWxzID0gcmVxdWlyZSgnLi4vdXRpbHMnKTtcbnZhciBhcHBTZXR0aW5ncyA9IHJlcXVpcmUoJy4uL2FwcFNldHRpbmdzJyk7XG5cbm1vZHVsZS5leHBvcnRzID0ge1xuICAgIGdldFBhdGhuYW1lOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBwYXRobmFtZSA9IHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZTtcbiAgICAgICAgdmFyIHBhdGhuYW1lQXJyYXkgPSBwYXRobmFtZS5zcGxpdCgnLycpO1xuXG4gICAgICAgIHJldHVybiBwYXRobmFtZUFycmF5W3BhdGhuYW1lQXJyYXkubGVuZ3RoIC0gMV07IC8vUmV0dXJuIHRoZSBsYXN0IGZpbGUgbmFtZVxuICAgIH0sXG4gICAgc3RhcnQ6IGZ1bmN0aW9uIChjYWxsYmFjaykge1xuICAgICAgICB3aW5kb3dbYXBwU2V0dGluZ3MuZ2xvYmFsVmFyTmFtZV0uaW5pdCA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gICAgICAgICAgICBjYWxsYmFjayhvcHRpb25zKTtcbiAgICAgICAgfTtcbiAgICB9LFxuICAgIHJlc29sdmVUb2tlbjogZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG4gICAgICAgIC8vR2V0IHRoZSB0b2tlbiBmcm9tIHRoZSBkZXZpY2UsIG9yIGdlbmVyYXRlIGEgbmV3IG9uZVxuICAgICAgICB2YXIgdG9rZW4gPSB3aW5kb3cuZGV2aWNlICYmIHdpbmRvdy5kZXZpY2UudXVpZCB8fCB1dGlscy5nZW5lcmF0ZVRva2VuKCk7XG4gICAgICAgIGNhbGxiYWNrKHRva2VuKTtcbiAgICB9XG59OyIsIi8qKlxuICogQ3JlYXRlZCBieSBOaWVrS3J1c2Ugb24gMS8yMC8xNi5cbiAqXG4gKiBNb2R1bGUgZm9yIGFkZGluZyBhbmQgcmVtb3ZpbmcgZXZlbnQgbGlzdGVuZXJzXG4gKi9cblxuLyoqXG4gKiBAZW51bSB7c3RyaW5nfVxuICovXG52YXIgZXZlbnRzID0ge1xuICAgIGFmdGVyQWRzSW5zZXJ0ZWQ6IFwiYWZ0ZXJBZHNJbnNlcnRlZFwiXG59O1xudmFyIGxpc3RlbmVycyA9IHt9O1xuXG4vKipcbiAqIENoZWNrIGlmIHRoZSBldmVudCBwYXNzZWQgaXMgdmFsaWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBldmVudE5hbWUgLSBOYW1lIG9mIHRoZSBldmVudFxuICovXG5mdW5jdGlvbiBjaGVja0V2ZW50VmFsaWQoZXZlbnROYW1lKSB7XG4gICAgaWYgKCFldmVudHMuaGFzT3duUHJvcGVydHkoZXZlbnROYW1lKSkge1xuICAgICAgICB0aHJvdyBldmVudE5hbWUgKyBcIiBpcyBub3QgYSB2YWxpZCBldmVudCBsaXN0ZW5lclwiO1xuICAgIH1cbn1cblxuLyoqXG4gKiBBZGQgYSBuZXcgZXZlbnQgbGlzdGVuZXJcbiAqIEBwYXJhbSB7ZXZlbnRzfSBldmVudCAtIFRoZSBuYW1lIG9mIHRoZSBldmVudCBsaXN0ZW5lciB0byBhZGQgYW4gZXZlbnQgZm9yXG4gKiBAcGFyYW0ge2Z1bmN0aW9ufSBjYWxsYmFjayAtIFRoZSBjYWxsYmFjayB0byBpbnZva2Ugd2hlbiB0aGUgZXZlbnQgaXMgY2FsbGVkXG4gKi9cbmZ1bmN0aW9uIGFkZExpc3RlbmVyKGV2ZW50LCBjYWxsYmFjaykge1xuICAgIGNoZWNrRXZlbnRWYWxpZChldmVudCk7XG5cbiAgICBpZiAobGlzdGVuZXJzW2V2ZW50XSkge1xuICAgICAgICBsaXN0ZW5lcnNbZXZlbnRdLnB1c2goY2FsbGJhY2spO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGxpc3RlbmVyc1tldmVudF0gPSBbY2FsbGJhY2tdO1xuICAgIH1cbn1cblxuLyoqXG4gKiBSZW1vdmUgYSBjZXJ0YWluIGV2ZW50IGxpc3RlbmVyXG4gKiBAcGFyYW0ge2V2ZW50c30gZXZlbnQgLSBUaGUgbmFtZSBvZiB0aGUgZXZlbnQgdG8gbGlzdGVuIHRvXG4gKiBAcGFyYW0ge2Z1bmN0aW9ufSBldmVudEhhbmRsZXIgLSBUaGUgZXZlbnRIYW5kbGVyIHRoYXQgaXMgYm91bmQgdG8gdGhpcyBsaXN0ZW5lciBhbmQgc2hvdWxkIGJlIHJlbW92ZWRcbiAqL1xuZnVuY3Rpb24gcmVtb3ZlTGlzdGVuZXIoZXZlbnQsIGV2ZW50SGFuZGxlcikge1xuICAgIGNoZWNrRXZlbnRWYWxpZChldmVudCk7XG5cbiAgICBpZiAobGlzdGVuZXJzW2V2ZW50XSAmJiBsaXN0ZW5lcnNbZXZlbnRdLmxlbmd0aCkge1xuICAgICAgICB2YXIgaW5kZXhPZkxpc3RlbmVyID0gbGlzdGVuZXJzW2V2ZW50XS5pbmRleE9mKGV2ZW50SGFuZGxlcik7XG4gICAgICAgIGlmIChpbmRleE9mTGlzdGVuZXIgPiAtMSkge1xuICAgICAgICAgICAgbGlzdGVuZXJzW2V2ZW50XS5zcGxpY2UoaW5kZXhPZkxpc3RlbmVyLCAxKTtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuLyoqXG4gKiBHZXQgdGhlIGV2ZW50IGhhbmRsZXJzIGZvciBhIGNlcnRhaW4gZXZlbnRcbiAqIEBwYXJhbSB7ZXZlbnRzfSBldmVudE5hbWUgLSBUaGUgbmFtZSBvZiB0aGUgZXZlbnQgdG8gZ2V0IGxpc3RlbmVycyBmb3JcbiAqL1xuZnVuY3Rpb24gZ2V0TGlzdGVuZXJzKGV2ZW50TmFtZSkge1xuICAgIHJldHVybiBsaXN0ZW5lcnNbZXZlbnROYW1lXTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgZXZlbnRzOiBldmVudHMsXG4gICAgYWRkTGlzdGVuZXI6IGFkZExpc3RlbmVyLFxuICAgIHJlbW92ZUxpc3RlbmVyOiByZW1vdmVMaXN0ZW5lcixcbiAgICBnZXRMaXN0ZW5lcnM6IGdldExpc3RlbmVyc1xufTsiLCIvKipcbiAqIE1haW4gZW50cnkgcG9pbnQgZm9yIHRoZSBhZCBsaWJyYXJ5LlxuICovXG52YXIgZGV2aWNlRGV0YWlscyA9IHJlcXVpcmUoJy4vZGV2aWNlL2RldmljZURldGVjdG9yJykuZ2V0RGV2aWNlRGV0YWlscygpLFxuICAgIGVudmlyb25tZW50ID0gcmVxdWlyZSgnLi9lbnYvZW52aXJvbm1lbnQnKSxcbiAgICBBZExpYiA9IHJlcXVpcmUoJy4vYWRMaWInKSxcbiAgICBhcHBTZXR0aW5ncyA9IHJlcXVpcmUoXCIuL2FwcFNldHRpbmdzXCIpO1xuXG52YXIgaXNJbml0aWFsaXplZCA9IGZhbHNlO1xuXG53aW5kb3dbYXBwU2V0dGluZ3MuZ2xvYmFsVmFyTmFtZV0gPSB7cmVhZHk6IGZhbHNlfTtcblxuaWYgKCFpc0luaXRpYWxpemVkICYmIGRldmljZURldGFpbHMuaXNWYWxpZFBsYXRmb3JtKSB7XG4gICAgZW52aXJvbm1lbnQuc3RhcnQoZnVuY3Rpb24ob3B0aW9ucyl7XG4gICAgICAgIHZhciBhZExpYiA9IG5ldyBBZExpYihlbnZpcm9ubWVudCwgb3B0aW9ucyk7XG4gICAgICAgIGFkTGliLmluaXQoKTtcbiAgICB9KTtcbiAgICBpc0luaXRpYWxpemVkID0gdHJ1ZTtcbn0iLCIvKipcbiAqIENyZWF0ZWQgYnkgTmlla0tydXNlIG9uIDEwLzE0LzE1LlxuICogTW9kdWxlIHRoYXQgY29udGFpbnMgaW5mb3JtYXRpb24gYWJvdXQgdGhlIGN1cnJlbnQgcGFnZVxuICogQG1vZHVsZSBwYWdlXG4gKi9cbnZhciBsb2dnZXIgPSByZXF1aXJlKFwiLi91dGlsL2xvZ2dlclwiKSxcbiAgICB1dGlscyA9IHJlcXVpcmUoXCIuL3V0aWxzXCIpLFxuICAgIEFkQ29udGFpbmVyID0gcmVxdWlyZShcIi4vYWRzL2FkQ29udGFpbmVyXCIpO1xuXG4vKipcbiAqIENhY2hlZCB2ZXJzaW9uIG9mIHRoZSB0b2tlblxuICogQHR5cGUge3N0cmluZ3xudWxsfVxuICovXG52YXIgdG9rZW4gPSBudWxsO1xudmFyIGlzUHJlbG9hZGluZ1Rva2VuID0gZmFsc2U7XG5cbi8vS2VlcCBhbiBhcnJheSBjb250YWluaW5nIGNhbGxiYWNrcyB0aGF0IHNob3VsZCBmaXJlIHdoZW4gdGhlIHBhZ2UgaXMgcmVhZHlcbnZhciBjYWxsYmFja3NPblJlYWR5ID0gW107XG5cbi8qKlxuICogRXZhbHVhdGVzIHhQYXRoIG9uIGEgcGFnZVxuICogQHBhcmFtIHtzdHJpbmd9IHhQYXRoU3RyaW5nIC0gVGhlIFhwYXRoIHN0cmluZyB0byBldmFsdWF0ZVxuICogQHJldHVybnMge0FycmF5LjxIVE1MRWxlbWVudD59IC0gQW4gYXJyYXkgb2YgdGhlIGZvdW5kIEhUTUwgZWxlbWVudHNcbiAqL1xuZnVuY3Rpb24geFBhdGgoeFBhdGhTdHJpbmcpIHtcbiAgICB2YXIgeFJlc3VsdCA9IGRvY3VtZW50LmV2YWx1YXRlKHhQYXRoU3RyaW5nLCBkb2N1bWVudCwgbnVsbCwgMCwgbnVsbCk7XG4gICAgdmFyIHhOb2RlcyA9IFtdO1xuICAgIHZhciB4UmVzID0geFJlc3VsdC5pdGVyYXRlTmV4dCgpO1xuICAgIHdoaWxlICh4UmVzKSB7XG4gICAgICAgIHhOb2Rlcy5wdXNoKHhSZXMpO1xuICAgICAgICB4UmVzID0geFJlc3VsdC5pdGVyYXRlTmV4dCgpO1xuICAgIH1cblxuICAgIHJldHVybiB4Tm9kZXM7XG59XG5cblxuLyoqXG4gKiBDaGVjayBpZiB0aGUgZW50aXJlIHBhZ2UgaXMgcmVhZHlcbiAqIEByZXR1cm5zIHtib29sZWFufSAtIFRydWUgaWYgdGhlIHBhZ2UgaXMgcmVhZHksIGZhbHNlIGlmIGl0IGlzbid0LlxuICovXG5mdW5jdGlvbiBpc1JlYWR5KCkge1xuICAgIHZhciBkb21SZWFkeSA9IGRvY3VtZW50LnJlYWR5U3RhdGUgIT09ICdsb2FkaW5nJztcbiAgICB2YXIgdG9rZW5SZWFkeSA9IHRva2VuICE9PSBudWxsO1xuXG4gICAgcmV0dXJuIChkb21SZWFkeSAmJiB0b2tlblJlYWR5KTtcbn1cblxuLyoqXG4gKiBFeGVjdXRlIGFsbCB0aGUgZnVuY3Rpb25zIHRoYXQgYXJlIHdhaXRpbmcgZm9yIHRoZSBwYWdlIHRvIGZpbmlzaCBsb2FkaW5nXG4gKi9cbmZ1bmN0aW9uIGV4ZWNXYWl0UmVhZHlGdW5jdGlvbnMoKSB7XG4gICAgaWYgKGlzUmVhZHkoKSkge1xuICAgICAgICBsb2dnZXIuaW5mbygnUGFnZSBpcyByZWFkeS4gRXhlY3V0aW5nICcgKyBjYWxsYmFja3NPblJlYWR5Lmxlbmd0aCArICcgZnVuY3Rpb25zIHRoYXQgYXJlIHdhaXRpbmcuJyk7XG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgY2FsbGJhY2tzT25SZWFkeS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgdmFyIGNhbGxiYWNrID0gY2FsbGJhY2tzT25SZWFkeVtpXTtcbiAgICAgICAgICAgIGNhbGxiYWNrKCk7XG4gICAgICAgIH1cbiAgICB9XG59XG5cbmZ1bmN0aW9uIHByZWxvYWRUb2tlbiAoZW52aXJvbm1lbnQpIHtcbiAgICBpc1ByZWxvYWRpbmdUb2tlbiA9IHRydWU7XG4gICAgZW52aXJvbm1lbnQucmVzb2x2ZVRva2VuKGZ1bmN0aW9uICh1c2VyVG9rZW4pIHtcbiAgICAgICAgdG9rZW4gPSB1c2VyVG9rZW47XG4gICAgICAgIGxvZ2dlci5pbmZvKCdVc2VyIHRyYWNraW5nIHRva2VuIHJlc29sdmVkJyk7XG4gICAgICAgIGV4ZWNXYWl0UmVhZHlGdW5jdGlvbnMoKTtcbiAgICB9KTtcbn1cblxuLyoqXG4gKiBSZXR1cm5zIGEgcHJvbWlzZSB0aGF0IHJlc29sdmVzIHdoZW4gdGhlIHBhZ2UgaXMgcmVhZHlcbiAqIEBwYXJhbSBmdW5jVG9FeGVjdXRlIC0gVGhlIGZ1bmN0aW9uIHRvIGV4ZWN1dGUgd2hlbiB0aGUgcGFnZSBpcyBsb2FkZWRcbiAqL1xuZnVuY3Rpb24gd2hlblJlYWR5KGZ1bmNUb0V4ZWN1dGUpIHtcbiAgICBpZiAoaXNSZWFkeSgpKSB7XG4gICAgICAgIGxvZ2dlci5pbmZvKCdQYWdlIGlzIGFscmVhZHkgbG9hZGVkLCBpbnN0YW50bHkgZXhlY3V0aW5nIScpO1xuICAgICAgICBmdW5jVG9FeGVjdXRlKCk7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBsb2dnZXIuaW5mbygnV2FpdGluZyBmb3IgcGFnZSB0byBiZSByZWFkeScpO1xuICAgIGNhbGxiYWNrc09uUmVhZHkucHVzaChmdW5jVG9FeGVjdXRlKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgLyoqXG4gICAgICogQ2hlY2sgd2hldGhlciB0aGUgcGFnZSBoYXMgcmVzcG9uc2l2ZSBkZXNpZ25cbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn0gaW5kaWNhdGluZyB3aGV0aGVyIHBhZ2UgaXMgcmVzcG9uc2l2ZSBvciBub3RcbiAgICAgKi9cbiAgICBpc1Jlc3BvbnNpdmU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIHZpZXdQb3J0TWV0YVRhZyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJtZXRhW25hbWU9dmlld3BvcnRdXCIpO1xuICAgICAgICByZXR1cm4gKHZpZXdQb3J0TWV0YVRhZyAhPT0gbnVsbCk7XG4gICAgfSxcbiAgICAvKipcbiAgICAgKiBHZXRzIHRoZSBhZGNvbnRhaW5lcnMgb24gdGhlIHBhZ2UgZnJvbSB0aGUgY29udGFpbmVyIHhQYXRoXG4gICAgICogQHBhcmFtIGFkVW5pdFNldHRpbmdzIHRoZSBzZXR0aW5ncyBmb3IgdGhlIGFkVW5pdCB0byBnZXQgdGhlIGNvbnRhaW5lciBvZlxuICAgICAqIEByZXR1cm5zIHtBcnJheS48T2JqZWN0Pn0gdGhlIEFkQ29udGFpbmVyIG9iamVjdCBvciBudWxsIGlmIG5vdCBmb3VuZFxuICAgICAqL1xuICAgIGdldEFkQ29udGFpbmVyczogZnVuY3Rpb24gKGFkVW5pdFNldHRpbmdzKSB7XG4gICAgICAgIHZhciBjb250YWluZXJzID0gYWRVbml0U2V0dGluZ3MuY29udGFpbmVycztcblxuICAgICAgICB2YXIgYWRDb250YWluZXJzID0gW107XG5cbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjb250YWluZXJzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICB2YXIgY29udGFpbmVyID0gY29udGFpbmVyc1tpXTtcblxuICAgICAgICAgICAgdmFyIGNvbnRhaW5lclhQYXRoID0gY29udGFpbmVyLnhQYXRoO1xuICAgICAgICAgICAgdmFyIGFkQ29udGFpbmVyRWxlbWVudHMgPSB4UGF0aChjb250YWluZXJYUGF0aCk7XG5cbiAgICAgICAgICAgIGlmICghYWRDb250YWluZXJFbGVtZW50cy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICBsb2dnZXIud2FybihcIkFkIGNvbnRhaW5lciB3aXRoIHhQYXRoOiBcXFwiXCIgKyBjb250YWluZXJYUGF0aCArIFwiXFxcIiBjb3VsZCBub3QgYmUgZm91bmQgb24gcGFnZVwiKTtcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKGFkQ29udGFpbmVyRWxlbWVudHMubGVuZ3RoID4gMSkge1xuICAgICAgICAgICAgICAgIGxvZ2dlci53YXJuKFwiQWQgY29udGFpbmVyIHdpdGggeFBhdGg6ICBcXFwiXCIgKyBjb250YWluZXJYUGF0aCArIFwiXFxcIiBoYXMgbXVsdGlwbGUgbWF0Y2hlc1wiKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgYWRDb250YWluZXJzLnB1c2gobmV3IEFkQ29udGFpbmVyKGNvbnRhaW5lciwgYWRDb250YWluZXJFbGVtZW50c1swXSkpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGFkQ29udGFpbmVycztcbiAgICB9LFxuICAgIC8qKlxuICAgICAqIHJlbW92ZSBhbiBlbGVtZW50IGZyb20gdGhlIGRvbVxuICAgICAqIEBwYXJhbSBkb21Ob2RlIHRoZSBlbGVtZW50IHRvIHJlbW92ZVxuICAgICAqL1xuICAgIHJlbW92ZUVsZW1lbnQ6IGZ1bmN0aW9uIChkb21Ob2RlKSB7XG4gICAgICAgIGRvbU5vZGUucGFyZW50RWxlbWVudC5yZW1vdmVDaGlsZChkb21Ob2RlKTtcbiAgICB9LFxuICAgIHhQYXRoOiB4UGF0aCxcbiAgICAvKipcbiAgICAgKiBHZXQgdGhlIE9mZmVyRW5naW5lIHRva2VuXG4gICAgICovXG4gICAgZ2V0VG9rZW46IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRva2VuO1xuICAgIH0sXG4gICAgcHJlbG9hZFRva2VuOiBwcmVsb2FkVG9rZW4sXG4gICAgYWRkRG9tUmVhZHlMaXN0ZW5lcjogZnVuY3Rpb24oZW52aXJvbm1lbnQpIHtcbiAgICAgICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGxvZ2dlci5pbmZvKCdET00gaXMgcmVhZHknKTtcbiAgICAgICAgICAgIGlmKCF0b2tlbiAmJiAhaXNQcmVsb2FkaW5nVG9rZW4pIHtcbiAgICAgICAgICAgICAgICBsb2dnZXIuaW5mbygnRE9NIHJlYWR5LCBsb2FkaW5nIHRva2VuJyk7XG4gICAgICAgICAgICAgICAgcHJlbG9hZFRva2VuKGVudmlyb25tZW50KTtcbiAgICAgICAgICAgICAgICByZXR1cm47IC8vV2UgZG9uJ3QgaGF2ZSB0byBjaGVjayBpZiB0aGVyZSdzIGZ1bmN0aW9ucyB3YWl0aW5nLCBjYXVzZSB0aGUgdG9rZW4gaXMgb25seSBqdXN0IGJlaW5nIHByZWxvYWRlZFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZXhlY1dhaXRSZWFkeUZ1bmN0aW9ucygpO1xuICAgICAgICB9KTtcbiAgICB9LFxuICAgIHdoZW5SZWFkeTogd2hlblJlYWR5XG59OyIsIi8qKlxuICogQ3JlYXRlZCBieSBOaWVrS3J1c2Ugb24gMTEvMTIvMTUuXG4gKiBVdGlsaXR5IG1vZHVsZSBjb250YWluaW5nIGhlbHBlciBmdW5jdGlvbnMgZm9yIGFqYXggcmVxdWVzdHNcbiAqXG4gKi9cbmZ1bmN0aW9uIGFwcGVuZFF1ZXJ5U3RyaW5nT3B0aW9ucyhyZXF1ZXN0VXJsLCBxdWVyeVN0cmluZ09wdGlvbnMpIHtcbiAgICByZXF1ZXN0VXJsICs9IFwiP1wiO1xuICAgIGZvciAodmFyIHByb3AgaW4gcXVlcnlTdHJpbmdPcHRpb25zKSB7XG4gICAgICAgIGlmIChxdWVyeVN0cmluZ09wdGlvbnMuaGFzT3duUHJvcGVydHkocHJvcCkpIHtcbiAgICAgICAgICAgIHJlcXVlc3RVcmwgKz0gcHJvcCArIFwiPVwiICsgcXVlcnlTdHJpbmdPcHRpb25zW3Byb3BdICsgXCImXCI7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvL1JlbW92ZSB0aGUgbGFzdCAmIGZyb20gdGhlIHN0cmluZ1xuICAgIHJlcXVlc3RVcmwgPSByZXF1ZXN0VXJsLnN1YnN0cigwLCByZXF1ZXN0VXJsLmxlbmd0aCAtIDEpO1xuICAgIHJldHVybiByZXF1ZXN0VXJsO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgICAvKipcbiAgICAgKiBAY2FsbGJhY2sgYWpheFN1Y2Nlc3NDYWxsYmFjayAtIFRoZSBjYWxsYmFjayB0byBpbnZva2Ugd2hlbiB0aGUgQWpheCBjYWxsIGlzIHN1Y2Nlc3NmdWxcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gLSBUaGUgZGF0YSByZWNlaXZlZCBmcm9tIHRoZSBBamF4IGNhbGxcbiAgICAgKi9cblxuICAgIC8qKlxuICAgICAqIEBjYWxsYmFjayBhamF4RXJyb3JDYWxsYmFjayAtIFRoZSBjYWxsYmFjayB0byBpbnZva2Ugd2hlbiB0aGUgQWpheCBjYWxsIHJldHVybnMgYW4gZXJyb3JcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gLSBUaGUgZXJyb3Igb2JqZWN0XG4gICAgICovXG5cbiAgICAvKipcbiAgICAgKiBAdHlwZWRlZiB7T2JqZWN0fSBBamF4T3B0aW9ucyAtIFRoZSByZXF1ZXN0IG9wdGlvbnNcbiAgICAgKiBAcHJvcGVydHkge3N0cmluZ30gdXJsIC0gVGhlIFVSTCBvZiB0aGUgZ2V0IHJlcXVlc3RcbiAgICAgKiBAcHJvcGVydHkge09iamVjdC48c3RyaW5nLCBzdHJpbmc+fSBbcXVlcnldIC0gVGhlIG9wdGlvbnMgdG8gYXBwZW5kIHRvIHRoZSBxdWVyeSBzdHJpbmdcbiAgICAgKiBAcHJvcGVydHkge2FqYXhTdWNjZXNzQ2FsbGJhY2t9IHN1Y2Nlc3MgLSBUaGUgY2FsbGJhY2sgdG8gaW52b2tlIHdoZW4gdGhlIGFqYXggY2FsbCBzdWNjZWVkc1xuICAgICAqIEBwcm9wZXJ0eSB7YWpheEVycm9yQ2FsbGJhY2t9IGVycm9yIC0gVGhlIGNhbGxiYWNrIHRvIGludm9rZSB3aGVuIHRoZSBhamF4IGNhbGwgcmV0dXJucyBhbiBlcnJvclxuICAgICAqL1xuXG4gICAgLyoqXG4gICAgICogRG8gYSBHRVQgcmVxdWVzdFxuICAgICAqIEBwYXJhbSB7QWpheE9wdGlvbnN9IG9wdGlvbnMgLSBUaGUgb3B0aW9uc1xuICAgICAqL1xuICAgIGdldDogZnVuY3Rpb24gKG9wdGlvbnMpIHtcbiAgICAgICAgdmFyIHJlcXVlc3QgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcblxuICAgICAgICB2YXIgcmVxdWVzdFVybCA9IGFwcGVuZFF1ZXJ5U3RyaW5nT3B0aW9ucyhvcHRpb25zLnVybCwgb3B0aW9ucy5xdWVyeSk7XG4gICAgICAgIHJlcXVlc3Qub3BlbignZ2V0JywgcmVxdWVzdFVybCk7XG5cbiAgICAgICAgcmVxdWVzdC5vbmxvYWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBvcHRpb25zLnN1Y2Nlc3MoSlNPTi5wYXJzZShyZXF1ZXN0LnJlc3BvbnNlVGV4dCkpO1xuICAgICAgICB9O1xuXG4gICAgICAgIHJlcXVlc3Qub25lcnJvciA9IGZ1bmN0aW9uIChwcm9ncmVzc0V2ZW50KSB7XG4gICAgICAgICAgICBvcHRpb25zLmVycm9yKHByb2dyZXNzRXZlbnQpO1xuICAgICAgICB9O1xuXG4gICAgICAgIHJlcXVlc3Quc2VuZCgpO1xuICAgIH1cbn07IiwiLyoqXG4gKiBDcmVhdGVkIGJ5IE5pZWtLcnVzZSBvbiAxMC8xNi8xNS5cbiAqIENvbnRhaW5zIGFwcCB3aWRlIGVudW1lcmF0aW9uc1xuICovXG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgICAvKipcbiAgICAgKiBUaGUgZW51bSBmb3IgdGhlIGxvZ0xldmVsXG4gICAgICogQHJlYWRvbmx5XG4gICAgICogQGVudW0ge251bWJlcn1cbiAgICAgKi9cbiAgICBsb2dMZXZlbDoge1xuICAgICAgICBvZmY6IDAsXG4gICAgICAgIGRlYnVnOiAxLFxuICAgICAgICB3YXJuOiAyLFxuICAgICAgICBlcnJvcjogM1xuICAgIH0sXG4gICAgLyoqXG4gICAgICogVGhlIGVudW0gZm9yIHRoZSBsb2dUeXBlXG4gICAgICogQHJlYWRvbmx5XG4gICAgICogQGVudW0ge3N0cmluZ31cbiAgICAgKi9cbiAgICBsb2dUeXBlOiB7XG4gICAgICAgIGluZm86IFwiSU5GT1wiLFxuICAgICAgICB3YXJuaW5nOiBcIldBUk5JTkdcIixcbiAgICAgICAgZXJyb3I6IFwiRVJST1JcIixcbiAgICAgICAgd3RmOiBcIkZBVEFMXCJcbiAgICB9XG59OyIsIi8qKlxuICogQ3JlYXRlZCBieSBOaWVrS3J1c2Ugb24gMTAvMTQvMTUuXG4gKiBIZWxwZXIgbW9kdWxlIGZvciBsb2dnaW5nIHB1cnBvc2VzXG4gKiBAbW9kdWxlIHV0aWwvbG9nZ2VyXG4gKi9cbnZhciBhcHBTZXR0aW5ncyA9IHJlcXVpcmUoJy4uL2FwcFNldHRpbmdzJyksXG4gICAgZW51bWVyYXRpb25zID0gcmVxdWlyZSgnLi4vdXRpbC9lbnVtZXJhdGlvbnMnKTtcblxuZnVuY3Rpb24gaW5pdCgpIHtcbiAgICAvL0NoZWNrIGlmIHRoZSBsb2dnZXIgZXhpc3RzXG4gICAgaWYgKCF3aW5kb3dbYXBwU2V0dGluZ3MubG9nZ2VyVmFyXSkge1xuICAgICAgICB2YXIgTG9nZ2VyID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdGhpcy5sb2dzID0gW107XG4gICAgICAgIH07XG5cbiAgICAgICAgTG9nZ2VyLnByb3RvdHlwZS5qc29uID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmV0dXJuIEpTT04uc3RyaW5naWZ5KHRoaXMubG9ncyk7XG4gICAgICAgIH07XG5cbiAgICAgICAgTG9nZ2VyLnByb3RvdHlwZS5wcmludCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBzdHJpbmcgPSBcIlwiO1xuXG4gICAgICAgICAgICB2YXIgY29uc29sZVJlZiA9IGNvbnNvbGU7XG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMubG9ncy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIHZhciBsb2cgPSB0aGlzLmxvZ3NbaV07XG5cbiAgICAgICAgICAgICAgICBjb25zb2xlUmVmLmxvZyh0b0ZyaWVuZGx5U3RyaW5nKGxvZykpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gc3RyaW5nO1xuICAgICAgICB9O1xuXG4gICAgICAgIHdpbmRvd1thcHBTZXR0aW5ncy5sb2dnZXJWYXJdID0gbmV3IExvZ2dlcigpO1xuICAgIH1cbn1cblxuLyoqXG4gKiBDcmVhdGUgYSBmcmllbmRseSBzdHJpbmcgb3V0IG9mIGEgbG9nIGVudHJ5XG4gKiBAcGFyYW0gbG9nRW50cnkgLSBUaGUgTG9nRW50cnkgdG8gY3JlYXRlIGEgZnJpZW5kbHkgc3RyaW5nIGZvclxuICogQHJldHVybnMge3N0cmluZ30gLSB0aGUgZnJpZW5kbHkgc3RyaW5nIG9mIHRoZSBMb2dFbnRyeVxuICovXG5mdW5jdGlvbiB0b0ZyaWVuZGx5U3RyaW5nKGxvZ0VudHJ5KSB7XG4gICAgcmV0dXJuIFwiW1BNX05hdGl2ZV9BZHMgXCIgKyBsb2dFbnRyeS50eXBlICsgXCJdIFwiICsgbG9nRW50cnkudGltZSArIFwiIC0gXCIgKyBsb2dFbnRyeS50ZXh0O1xufVxuXG4vKipcbiAqIFB1c2ggYSBsb2dFbnRyeSB0byB0aGUgYXJyYXkgb2YgbG9ncyBhbmQgb3V0cHV0IGl0IHRvIHRoZSBjb25zb2xlXG4gKiBAcGFyYW0gbG9nRW50cnkgVGhlIGxvZ0VudHJ5IHRvIHByb2Nlc3NcbiAqL1xuZnVuY3Rpb24gcHVzaExvZ0VudHJ5KGxvZ0VudHJ5KSB7XG4gICAgdmFyIGxvZ2dlciA9IHdpbmRvd1thcHBTZXR0aW5ncy5sb2dnZXJWYXJdO1xuICAgIGlmIChsb2dnZXIpIHtcbiAgICAgICAgbG9nZ2VyLmxvZ3MucHVzaChsb2dFbnRyeSk7XG5cbiAgICAgICAgaWYgKHdpbmRvdy5jb25zb2xlKSB7XG4gICAgICAgICAgICBpZiAodHlwZW9mIGNvbnNvbGUuZXJyb3IgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgY29uc29sZS53YXJuID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKGxvZ0VudHJ5LnR5cGUpIHtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBlbnVtZXJhdGlvbnMubG9nVHlwZS53dGY6XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKHRvRnJpZW5kbHlTdHJpbmcobG9nRW50cnkpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIGVudW1lcmF0aW9ucy5sb2dUeXBlLmVycm9yOlxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFwcFNldHRpbmdzLmxvZ0xldmVsIDw9IGVudW1lcmF0aW9ucy5sb2dMZXZlbC5lcnJvciAmJiBhcHBTZXR0aW5ncy5sb2dMZXZlbCA+IGVudW1lcmF0aW9ucy5sb2dMZXZlbC5vZmYpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKHRvRnJpZW5kbHlTdHJpbmcobG9nRW50cnkpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIGVudW1lcmF0aW9ucy5sb2dUeXBlLndhcm5pbmc6XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoYXBwU2V0dGluZ3MubG9nTGV2ZWwgPD0gZW51bWVyYXRpb25zLmxvZ0xldmVsLndhcm4gJiYgYXBwU2V0dGluZ3MubG9nTGV2ZWwgPiBlbnVtZXJhdGlvbnMubG9nTGV2ZWwub2ZmKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS53YXJuKHRvRnJpZW5kbHlTdHJpbmcobG9nRW50cnkpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFwcFNldHRpbmdzLmxvZ0xldmVsIDw9IGVudW1lcmF0aW9ucy5sb2dMZXZlbC5kZWJ1ZyAmJiBhcHBTZXR0aW5ncy5sb2dMZXZlbCA+IGVudW1lcmF0aW9ucy5sb2dMZXZlbC5vZmYpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0b0ZyaWVuZGx5U3RyaW5nKGxvZ0VudHJ5KSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRvRnJpZW5kbHlTdHJpbmcobG9nRW50cnkpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn1cblxuLyoqXG4gKiBHZXQgdGhlIGN1cnJlbnQgdGltZSBhcyBhIHN0cmluZ1xuICogQHJldHVybnMge3N0cmluZ30gLSB0aGUgY3VycmVudCB0aW1lIGluIGEgaGg6bW06c3Mgc3RyaW5nXG4gKi9cbmZ1bmN0aW9uIGdldEN1cnJlbnRUaW1lU3RyaW5nKCkge1xuICAgIHZhciB0b2RheSA9IG5ldyBEYXRlKCk7XG4gICAgdmFyIGhoID0gdG9kYXkuZ2V0SG91cnMoKTtcbiAgICB2YXIgbW0gPSB0b2RheS5nZXRNaW51dGVzKCk7IC8vSmFudWFyeSBpcyAwXG4gICAgdmFyIHNzID0gdG9kYXkuZ2V0U2Vjb25kcygpO1xuICAgIHZhciBtcyA9IHRvZGF5LmdldE1pbGxpc2Vjb25kcygpO1xuXG4gICAgaWYgKGhoIDwgMTApIHtcbiAgICAgICAgaGggPSAnMCcgKyBoaDtcbiAgICB9XG5cbiAgICBpZiAobW0gPCAxMCkge1xuICAgICAgICBtbSA9ICcwJyArIG1tO1xuICAgIH1cblxuICAgIGlmIChzcyA8IDEwKSB7XG4gICAgICAgIHNzID0gJzAnICsgc3M7XG4gICAgfVxuXG4gICAgaWYgKG1zIDwgMTApIHtcbiAgICAgICAgbXMgPSAnMCcgKyBtcztcbiAgICB9XG5cbiAgICByZXR1cm4gaGggKyBcIjpcIiArIG1tICsgXCI6XCIgKyBzcyArIFwiOlwiICsgbXM7XG59XG5cbi8qKlxuICogQHR5cGVkZWYge09iamVjdH0gTG9nRW50cnkgLSBBIGxvZ2dpbmcgZW50cnkgb2JqZWN0XG4gKiBAcGFyYW0ge3N0cmluZ30gdGltZSAtIFRoZSB0aW1lIG9mIHRoZSBsb2cgYXMgYSBzdHJpbmdcbiAqIEBwYXJhbSB7dGV4dH0gdGV4dCAtIFRoZSB0ZXh0IG9mIHRoZSBsb2dcbiAqL1xuXG4vKipcbiAqIENyZWF0ZSBhIG5ldyBMb2dFbnRyeSBvYmplY3RcbiAqIEBwYXJhbSB7c3RyaW5nfSBsb2dUeXBlIC0gdGhlIHR5cGUgb2YgbG9nXG4gKiBAcGFyYW0ge3N0cmluZ30gbG9nVGV4dCAtIFRoZSB0ZXh0IG9mIHRoZSBsb2dcbiAqL1xuZnVuY3Rpb24gY3JlYXRlTG9nRW50cnkobG9nVHlwZSwgbG9nVGV4dCkge1xuICAgIHZhciBsb2dnZXIgPSB3aW5kb3dbYXBwU2V0dGluZ3MubG9nZ2VyVmFyXTtcbiAgICBpZighbG9nZ2VyKSB7XG4gICAgICAgIGluaXQoKTsgLy9BbHdheXMgaW5pdGlhbGl6ZSBvbiBvdXIgZmlyc3QgbG9nIGVudHJ5XG4gICAgfVxuXG4gICAgdmFyIGxvZyA9IHtcbiAgICAgICAgdHlwZTogbG9nVHlwZSxcbiAgICAgICAgdGltZTogZ2V0Q3VycmVudFRpbWVTdHJpbmcoKSxcbiAgICAgICAgdGV4dDogbG9nVGV4dFxuICAgIH07XG5cbiAgICBwdXNoTG9nRW50cnkobG9nKTtcbn1cblxuXG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgbmV3IGluZm8gbG9nXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGxvZ1RleHQgLSB0aGUgdGV4dCB0aGUgbG9nIHNob3VsZCBjb250YWluXG4gICAgICovXG4gICAgaW5mbzogZnVuY3Rpb24gKGxvZ1RleHQpIHtcbiAgICAgICAgY3JlYXRlTG9nRW50cnkoZW51bWVyYXRpb25zLmxvZ1R5cGUuaW5mbywgbG9nVGV4dCk7XG4gICAgfSxcbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgbmV3IHdhcm5pbmcgbG9nXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGxvZ1RleHQgLSB0aGUgdGV4dCB0aGUgbG9nIHNob3VsZCBjb250YWluXG4gICAgICovXG4gICAgd2FybjogZnVuY3Rpb24gKGxvZ1RleHQpIHtcbiAgICAgICAgY3JlYXRlTG9nRW50cnkoZW51bWVyYXRpb25zLmxvZ1R5cGUud2FybmluZywgbG9nVGV4dCk7XG4gICAgfSxcbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgbmV3IGVycm9yIGxvZ1xuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBsb2dUZXh0IC0gdGhlIHRleHQgdGhlIGxvZyBzaG91bGQgY29udGFpblxuICAgICAqL1xuICAgIGVycm9yOiBmdW5jdGlvbiAobG9nVGV4dCkge1xuICAgICAgICBjcmVhdGVMb2dFbnRyeShlbnVtZXJhdGlvbnMubG9nVHlwZS5lcnJvciwgbG9nVGV4dCk7XG4gICAgfSxcbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgbmV3IFdURiAoV2hhdCBhIHRlcnJpYmxlIGZhaWx1cmUpIGxvZ1xuICAgICAqIFRoZXNlIHNob3VsZCBuZXZlciBvY2N1ciBpbiB0aGUgYXBwbGljYXRpb25cbiAgICAgKiBXaWxsIGFsd2F5cyBiZSBvdXRwdXR0ZWQgZXZlbiBpZiB0aGUgbG9nTGV2ZWwgaXMgMFxuICAgICAqIEBwYXJhbSBsb2dUZXh0XG4gICAgICovXG4gICAgd3RmOiBmdW5jdGlvbiAobG9nVGV4dCkge1xuICAgICAgICBjcmVhdGVMb2dFbnRyeShlbnVtZXJhdGlvbnMubG9nVHlwZS53dGYsIGxvZ1RleHQpO1xuICAgIH1cbn07XG4iLCIvKipcbiAqIENyZWF0ZWQgYnkgTmlla0tydXNlIG9uIDEwLzUvMTUuXG4gKlxuICogVGhpcyBtb2R1bGUgY29udGFpbnMgVXRpbGl0eSBmdW5jdGlvbnMgdGhhdCBjYW4gYmUgdXNlZCB0aHJvdWdob3V0IHRoZSBwcm9qZWN0LlxuICovXG5cbi8qKlxuICogT2JqZWN0IGNvbnRhaW5pbmcgdXRpbGl0eSBmdW5jdGlvbnNcbiAqL1xudmFyIHV0aWxzID0ge307XG5cbi8qKlxuICogUmVwbGFjZXMgbWFjcm9zIGluIGEgc3RyaW5nIHdpdGggYWN0dWFsIHZhbHVlc1xuICogQHBhcmFtIHtzdHJpbmd9IHN0clRvRm9ybWF0IC0gVGhlIHN0cmluZyB0byBmb3JtYXRcbiAqIEByZXR1cm5zIHtzdHJpbmd9IHRoZSBmb3JtYXR0ZWQgc3RyaW5nXG4gKi9cbnV0aWxzLmZvcm1hdFN0cmluZyA9IGZ1bmN0aW9uIChzdHJUb0Zvcm1hdCkge1xuICAgIHZhciBzID0gc3RyVG9Gb3JtYXQ7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBhcmd1bWVudHMubGVuZ3RoIC0gMTsgaSsrKSB7XG4gICAgICAgIHZhciByZWcgPSBuZXcgUmVnRXhwKFwiXFxcXHtcIiArIGkgKyBcIlxcXFx9XCIsIFwiZ21cIik7XG4gICAgICAgIHMgPSBzLnJlcGxhY2UocmVnLCBhcmd1bWVudHNbaSArIDFdKTtcbiAgICB9XG5cbiAgICByZXR1cm4gcztcbn07XG5cbi8qKlxuICogQ29udmVydHMgYSBzdHJpbmcgdG8gYSBzdHJpbmcgYXJyYXkgaWYgdGhlIHBhc3NlZCBwYXJhbWV0ZXIgaXMgYSBzdHJpbmcuXG4gKiBJZiB0aGUgcGFzc2VkIHBhcmFtZXRlciBpcyBhbHJlYWR5IGEgc3RyaW5nIGFycmF5LCB0aGUgZnVuY3Rpb24gd2lsbCByZXR1cm4gaXQuXG4gKiBAcGFyYW0ge3N0cmluZ3xzdHJpbmdbXX0gc3RyaW5nT3JBcnJheSAtIFRoZSBzdHJpbmcgdG8gY29udmVydCB0byBhbiBhcnJheVxuICogQHJldHVybnMge0FycmF5LjxzdHJpbmc+fSAtIFRoZSBzdHJpbmcgYXJyYXlcbiAqL1xudXRpbHMuc3RyaW5nVG9BcnJheSA9IGZ1bmN0aW9uIChzdHJpbmdPckFycmF5KSB7XG4gICAgaWYgKEFycmF5LmlzQXJyYXkoc3RyaW5nT3JBcnJheSkpXG4gICAgICAgIHJldHVybiBzdHJpbmdPckFycmF5O1xuXG4gICAgaWYgKHR5cGVvZiBzdHJpbmdPckFycmF5ID09PSAnc3RyaW5nJyB8fCBzdHJpbmdPckFycmF5IGluc3RhbmNlb2YgU3RyaW5nKSB7XG4gICAgICAgIC8vRml4IGludG8gYXJyYXlcbiAgICAgICAgc3RyaW5nT3JBcnJheSA9IFtzdHJpbmdPckFycmF5XTtcbiAgICB9IGVsc2Uge1xuICAgICAgICB0aHJvdyBzdHJpbmdPckFycmF5LnRvU3RyaW5nKCkgKyBcIiBpcyBub3QgYSB2YWxpZCBzdHJpbmcgb3Igc3RyaW5nIGFycmF5XCI7XG4gICAgfVxuXG4gICAgcmV0dXJuIHN0cmluZ09yQXJyYXk7XG59O1xuXG4vKipcbiAqIEdlbmVyYXRlIGEgcmFuZG9tIHRva2VuIGZvciB0aGUgb2ZmZXJ3YWxsXG4gKiBUT0RPOiBtaWdodCBuZWVkIHNvbWUgaW1wcm92ZW1lbnRcbiAqIEByZXR1cm5zIHtzdHJpbmd9IC0gQSB1bmlxdWUgdXNlciB0b2tlblxuICovXG51dGlscy5nZW5lcmF0ZVRva2VuID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBwcmVmaXggPSBcIm9mZmVyZW5naW5lX1wiO1xuICAgIHZhciBub3cgPSBEYXRlLm5vdygpO1xuICAgIHZhciByYW5kb20gPSBNYXRoLnJhbmRvbSgpLnRvU3RyaW5nKDM2KS5zdWJzdHJpbmcoNyk7XG4gICAgcmV0dXJuIHByZWZpeCArIG5vdyArIHJhbmRvbTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gdXRpbHM7XG4iXX0=
